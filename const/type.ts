export const TYPE = {
    SUCCESS: "success",
    ERROR: "error",
    WARNING: "warning",
    FULLTIME: "FULLTIME",
    PARTTIME:  "PARTTIME",
    INTERNSHIP: "INTERNSHIP",

    MON: 'MON',
    TUE: 'TUE',
    WED: 'WED',
    THU: 'THU',
    FRI: 'FRI',
    SAT: 'SAT',
    SUN: 'SUN',

    MOR: 'MOR',
    AFT: 'AFT',
    EVN: 'EVN',
    
    TOP: 'TOP',
    HIGHLIGHT: 'HIGHLIGHT',

    EVENT: 'EVENT',
    NORMAL: 'NORMAL'
}