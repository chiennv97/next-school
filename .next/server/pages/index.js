module.exports =
(function() {
var exports = {};
exports.id = 405;
exports.ids = [405];
exports.modules = {

/***/ 5318:
/***/ (function(module) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ 862:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var _typeof = __webpack_require__(8);

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

module.exports = _interopRequireWildcard;

/***/ }),

/***/ 8:
/***/ (function(module) {

function _typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ 459:
/***/ (function(module) {

// Exports
module.exports = {
	"banner": "Banner_banner__2PJ1Y",
	"imageBanner": "Banner_imageBanner__2g9Ic",
	"info-in-banner": "Banner_info-in-banner__1P3JO",
	"banner-logo": "Banner_banner-logo__2YBzy",
	"text-banner": "Banner_text-banner__2Uv3X",
	"info": "Banner_info__26QEI",
	"search-body-event": "Banner_search-body-event__2-zQU",
	"school-description": "Banner_school-description__16R7w",
	"buttonPlayGame": "Banner_buttonPlayGame__2qWLR",
	"wapperButtonPlayGame": "Banner_wapperButtonPlayGame__2xUe8"
};


/***/ }),

/***/ 4118:
/***/ (function(module) {

// Exports
module.exports = {
	"header": "Header_header__3V_Bw",
	"triagle-up": "Header_triagle-up__1nZ_F",
	"triagle-shadow": "Header_triagle-shadow__1gJ3V",
	"search_box_container": "Header_search_box_container__1GF6o",
	"logo": "Header_logo__1haEc",
	"directPage": "Header_directPage__2RPCF",
	"function": "Header_function__1R0fT",
	"label-function": "Header_label-function__2jPbk",
	"label_name": "Header_label_name__5CK-Q",
	"ant-popover-open": "Header_ant-popover-open__2fbCE",
	"borderLeft": "Header_borderLeft__n86jG",
	"labelLogin": "Header_labelLogin__KFGUT",
	"profile-function": "Header_profile-function__2yEXg",
	"ant-badge-multiple-words": "Header_ant-badge-multiple-words__30iOf",
	"ant-popover-inner": "Header_ant-popover-inner__2hoLr",
	"wapper-education": "Header_wapper-education__3oLN0"
};


/***/ }),

/***/ 335:
/***/ (function(module) {

// Exports
module.exports = {
	"homeJob": "TopJobEm_homeJob__cC_1u",
	"ant-skeleton": "TopJobEm_ant-skeleton__13j_Z",
	"ant-skeleton-header": "TopJobEm_ant-skeleton-header__BNO5F",
	"ant-skeleton-avatar-lg": "TopJobEm_ant-skeleton-avatar-lg__3phFP",
	"hJItemTop": "TopJobEm_hJItemTop__3lzu4",
	"imgJob": "TopJobEm_imgJob__akczn",
	"region": "TopJobEm_region__3hh-j",
	"salary": "TopJobEm_salary__2KDLm",
	"salaryLabel": "TopJobEm_salaryLabel__H-Phq",
	"topBadge": "TopJobEm_topBadge__32iMR",
	"jobContent": "TopJobEm_jobContent__1mfpf",
	"aNameEmployer": "TopJobEm_aNameEmployer__13NOh",
	"jD": "TopJobEm_jD__153Ur",
	"lC": "TopJobEm_lC__1M9FI",
	"marquee": "TopJobEm_marquee__29DiE",
	"h-j-item": "TopJobEm_h-j-item__lIG4M",
	"image-employer": "TopJobEm_image-employer__3sQ0F",
	"img-job": "TopJobEm_img-job__1JgZq",
	"job-content": "TopJobEm_job-content__3iJMR"
};


/***/ }),

/***/ 819:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "O4": function() { return /* binding */ JobType; },
/* harmony export */   "r_": function() { return /* binding */ IptLetter; },
/* harmony export */   "yR": function() { return /* binding */ NotUpdate; }
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = (react__WEBPACK_IMPORTED_MODULE_0___default().createElement);

function JobType(props) {
  let style = {
    color: 'black',
    backgroundColor: 'white',
    fontSize: props.fontSize ? props.fontSize : '0.8em',
    textAlign: 'center',
    width: props.width ? props.width : '75px',
    display: 'inline-block',
    borderRadius: '5px'
  };
  let label;

  switch (props.children) {
    case 'FULLTIME':
      style.color = 'white';
      style.backgroundColor = '#06bbe4';
      label = 'FULL-TIME';
      break;

    case 'PARTTIME':
      style.color = 'white';
      style.backgroundColor = '#00b33c';
      label = 'PART-TIME';
      break;

    case 'INTERNSHIP':
      style.color = 'white';
      style.backgroundColor = '#ff9933';
      label = 'THỰC TẬP';
      break;

    default:
      break;
  }

  return __jsx("div", {
    style: style
  }, label);
}
const IptLetter = props => {
  return __jsx("span", {
    className: "important-letter"
  }, " " + props.value + " ");
};
const NotUpdate = props => {
  const {
    msg
  } = props;
  return __jsx("span", {
    style: {
      fontSize: '0.8 rem',
      fontStyle: 'italic'
    }
  }, msg ? msg : 'Chưa cập nhật');
};

/***/ }),

/***/ 7030:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "r": function() { return /* binding */ REDUX; },
/* harmony export */   "A": function() { return /* binding */ REDUX_SAGA; }
/* harmony export */ });
const REDUX = {
  AUTHEN: {
    FAIL_AUTHEN: "FAIL_AUTHEN",
    EXACT_AUTHEN: "EXACT_AUTHEN"
  },
  ANNOUNCEMENTS: {
    GET_ANNOUNCEMENTS: "GET_ANNOUNCEMENTS",
    GET_ANNOUNCEMENT_DETAIL: "GET_ANNOUNCEMENT_DETAIL"
  },
  EMPLOYER_DETAIL: {
    GET_EMPLOYER_DETAIL: "GET_EMPLOYER_DETAIL"
  },
  EMPLOYER_MORE_JOB: {
    GET_EMPLOYER_MORE_JOB: "EMPLOYER_MORE_JOB",
    SET_LOADING_MORE_JOB: "SET_LOADING_MORE_JOB"
  },
  SIMILAR_JOB: {
    GET_SIMILAR_JOB: "GET_SIMILAR_JOB",
    SET_LOADING_SIMILAR_JOB: "SET_LOADING_SIMILAR_JOB"
  },
  EVENT: {
    NOT_AVAILABLE: 'EVENT_IS_NOT_AVAILABLE',
    START: "CHECK_EVENT_STATUS",
    DETAIL: "GET_EVENT",
    LOGO_SCHOOL: "SET_LOGO_SCHOOL",
    LOADING: "SET_LOADING",
    JOB: {
      HOT: "GET_EVENT_HOT_JOBS",
      HOT_LOADING: "SET_LOADING_EVENT_HOT_JOB",
      HOT_CURRENT_PAGE: "SET_CURRENT_PAGE_EVENT_HOT_JOB",
      NORMAL: "GET_EVENT_NORMAL_JOBS",
      NORMAL_CURRENT_PAGE: "GET_EVENT_NORMAL_JOBS_CURRENT_PAGE",
      ALL: "GET_EVENT_ALL_JOBS",
      DETAIL: "GET_EVENT_JOB_DETAIL",
      SAVE: "SAVE_EVENT_JOB",
      BRANCH: 'GET_LIST_BRANCH_JOB',
      SEARCH: 'GET_JOBS_RESULTS'
    },
    EMPLOYER: {
      ALL: "GET_ALL_EMPLOYER",
      TOP: "GET_TOP_EMPLOYER",
      BANNER: 'GET_BANNER_EMPLOYER',
      MORE_JOB: 'GET_EMPLOYER_MORE_JOB'
    }
  },
  HIGH_LIGHT: {
    GET_HIGH_LIGHT_JOB: "GET_HIGH_LIGHT_JOB",
    SET_LOADING_HIGH_LIGHT_JOB: "SET_LOADING_HIGH_LIGHT_JOB"
  },
  IN_DAY: {
    GET_IN_DAY_JOB: "GET_IN_DAY_JOB"
  },
  HOT_JOB: {
    GET_HOT_JOB: "GET_HOT_JOB",
    SET_LOADING_HOT_JOB: "SET_LOADING_HOT_JOB"
  },
  ALL_JOB: {
    GET_ALL_JOB: "GET_ALL_JOB",
    SET_LOADING_ALL_JOB: "SET_LOADING_ALL_JOB"
  },
  JOB_DETAIL: {
    GET_JOB_DETAIL: "GET_JOB_DETAIL"
  },
  JOB_RESULT: {
    GET_JOB_RESULT: "GET_JOB_RESULT",
    SEARCH_JOB_DTO: "SEARCH_JOB_DTO",
    SET_FILTER_JOB_TYPE: "SET_FILTER_JOB_TYPE",
    SET_FILTER_LIST_SHIFT: "SET_FILTER_LIST_SHIFT",
    SET_FILTER_LIST_DAY: "SET_FILTER_LIST_DAY",
    SET_FILTER_AREA: "SET_FILTER_AREA",
    SET_FILTER_JOBNAME: "SET_FILTER_JOBNAME",
    SET_LOADING_RESULT: "SET_LOADING_RESULT",
    SET_FILTER: "SET_FILTER"
  },
  SAVED_JOB: {
    GET_SAVED_JOB: "GET_SAVED_JOB",
    SET_LOADING_SAVE_JOB: "SET_LOADING_SAVE_JOB"
  },
  HISTORY_APPLY: {
    GET_HISTORY_APPLY: "GET_HISTORY_APPLY",
    SET_LOADING_HISTORY_APPLY: "SET_LOADING_HISTORY_APPLY"
  },
  MAP: {
    SET_MAP_STATE: "SET_MAP_STATE"
  },
  MOBILE_STATE: {
    SET_MOBILE_STATE: "SET_MOBILE_STATE"
  },
  NOTI: {
    GET_NOTI: "GET_NOTI"
  },
  PERSON_INFO: {
    GET_SHORT_PERSON_INFO: "PERSONAL_INFO",
    GET_FULL_PERSON_INFO: 'FULL_INFO'
  },
  POPUP: {
    OPEN_POPUP: "OPEN_POPUP",
    CLOSE_POPUP: "CLOSE_POPUP"
  },
  SIDE_BAR: {
    OPEN_SIDE_BAR: "OPEN_SIDE_BAR",
    CLOSE_SIDE_BAR: "CLOSE_SIDE_BAR"
  },
  CHAT_ROOM: {
    SET_CHAT_ROOM: "SET_CHAT_ROOM"
  },
  JOB_NAMES: {
    GET_JOB_NAMES: "GET_JOB_NAMES"
  },
  JOB_GROUPS: {
    GET_JOB_GROUPS: "GET_JOB_GROUPS"
  },
  REGIONS: {
    GET_REGIONS: "GET_REGIONS"
  }
};
const REDUX_SAGA = {
  EMPLOYER_DETAIL: {
    GET_EMPLOYER_DETAIL: "GET_EMPLOYER_DETAIL_DATA"
  },
  EMPLOYER_MORE_JOB: {
    GET_EMPLOYER_MORE_JOB: "GET_EMPLOYER_MORE_JOB_DATA"
  },
  SIMILAR_JOB: {
    GET_SIMILAR_JOB: "GET_SIMILAR_JOB_DATA"
  },
  HIGH_LIGHT: {
    GET_HIGH_LIGHT_DATA: "GET_HIGH_LIGHT_DATA"
  },
  EVENT: {
    DETAIL: "GET_EVENT_DATA",
    JOB: {
      HOT: "GET_EVENT_HOT_JOBS_DATA",
      HOT_LOADING: "SET_LOADING_EVENT_HOT_JOB_DATA",
      NORMAL: "GET_EVENT_NORMAL_JOBS_DATA",
      ALL: "GET_EVENT_ALL_JOBS_DATA",
      DETAIL: "GET_EVENT_JOB_DETAIL_DATA",
      SAVE: "SAVE_EVENT_JOB_DATA",
      BRANCH: 'GET_LIST_BRANCH_JOB_DATA',
      SEARCH: 'GET_JOBS_RESULTS_DATA'
    },
    EMPLOYER: {
      ALL: "GET_ALL_EMPLOYER_DATA",
      TOP: "GET_TOP_EMPLOYER_DATA",
      BANNER: 'GET_BANNER_EMPLOYER_DATA',
      MORE_JOB: 'GET_EMPLOYER_MORE_JOB_DATA'
    }
  },
  IN_DAY: {
    GET_IN_DAY_JOB: "GET_IN_DAY_JOB_DATA"
  },
  HOT_JOB: {
    GET_HOT_JOB: "GET_HOT_JOB_DATA"
  },
  ALL_JOB: {
    GET_ALL_JOB: "GET_ALL_JOB_DATA"
  },
  JOB_DETAIL: {
    GET_JOB_DETAIL: "GET_JOB_DETAI_DATA"
  },
  JOB_RESULT: {
    GET_JOB_RESULT: "GET_JOB_RESULT_DATA"
  },
  SAVED_JOB: {
    GET_SAVED_JOB: "GET_SAVED_JOB_DATA"
  },
  HISTORY_APPLY: {
    GET_HISTORY_APPLY: "GET_HISTORY_APPLY_DATA"
  },
  NOTI: {
    GET_NOTI: "GET_NOTI_DATA"
  },
  PERSON_INFO: {
    GET_SHORT_PERSON_INFO: "PERSONAL_INFO_DATA",
    GET_FULL_PERSON_INFO: 'GET_FULL_INFO_DATA'
  },
  JOB_NAMES: {
    GET_JOB_NAMES: "GET_JOB_NAMES_DATA"
  },
  JOB_GROUPS: {
    GET_JOB_GROUPS: "GET_JOB_GROUPS_DATA"
  },
  REGIONS: {
    GET_REGIONS: "GET_REGIONS_DATA"
  },
  ANNOUNCEMENTS: {
    GET_ANNOUNCEMENTS: "GET_ANNOUNCEMENTS_DATA",
    GET_ANNOUNCEMENT_DETAIL: "GET_ANNOUNCEMENT_DETAIL_DATA"
  }
};

/***/ }),

/***/ 4151:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "U4": function() { return /* binding */ PUBLIC_HOST; }
/* harmony export */ });
/* unused harmony exports AUTH_HOST, STUDENT_HOST */
const AUTH_HOST = process.env.REACT_APP_API_HOST + '/oauth2';
const STUDENT_HOST = process.env.REACT_APP_API_HOST + '/students';
const PUBLIC_HOST = 'https://api.works.vn:8443' + '/public';

/***/ }),

/***/ 8535:
/***/ (function() {

"use strict";


/***/ }),

/***/ 6071:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";
var __webpack_unused_export__;


var _interopRequireWildcard = __webpack_require__(862);

__webpack_unused_export__ = true;
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(9297));

var _router = __webpack_require__(1689);

var _router2 = __webpack_require__(2441);

var _useIntersection = __webpack_require__(5749);

const prefetched = {};

function prefetch(router, href, as, options) {
  if (true) return;
  if (!(0, _router.isLocalURL)(href)) return; // Prefetch the JSON page if asked (only in the client)
  // We need to handle a prefetch error here since we may be
  // loading with priority which can reject but we don't
  // want to force navigation since this is only a prefetch

  router.prefetch(href, as, options).catch(err => {
    if (false) {}
  });
  const curLocale = options && typeof options.locale !== 'undefined' ? options.locale : router && router.locale; // Join on an invalid URI character

  prefetched[href + '%' + as + (curLocale ? '%' + curLocale : '')] = true;
}

function isModifiedEvent(event) {
  const {
    target
  } = event.currentTarget;
  return target && target !== '_self' || event.metaKey || event.ctrlKey || event.shiftKey || event.altKey || // triggers resource download
  event.nativeEvent && event.nativeEvent.which === 2;
}

function linkClicked(e, router, href, as, replace, shallow, scroll, locale) {
  const {
    nodeName
  } = e.currentTarget;

  if (nodeName === 'A' && (isModifiedEvent(e) || !(0, _router.isLocalURL)(href))) {
    // ignore click for browser’s default behavior
    return;
  }

  e.preventDefault(); //  avoid scroll for urls with anchor refs

  if (scroll == null) {
    scroll = as.indexOf('#') < 0;
  } // replace state instead of push if prop is present


  router[replace ? 'replace' : 'push'](href, as, {
    shallow,
    locale,
    scroll
  }).then(success => {
    if (!success) return;

    if (scroll) {
      // FIXME: proper route announcing at Router level, not Link:
      document.body.focus();
    }
  });
}

function Link(props) {
  if (false) {}

  const p = props.prefetch !== false;
  const router = (0, _router2.useRouter)();
  const pathname = router && router.pathname || '/';

  const {
    href,
    as
  } = _react.default.useMemo(() => {
    const [resolvedHref, resolvedAs] = (0, _router.resolveHref)(pathname, props.href, true);
    return {
      href: resolvedHref,
      as: props.as ? (0, _router.resolveHref)(pathname, props.as) : resolvedAs || resolvedHref
    };
  }, [pathname, props.href, props.as]);

  let {
    children,
    replace,
    shallow,
    scroll,
    locale
  } = props; // Deprecated. Warning shown by propType check. If the children provided is a string (<Link>example</Link>) we wrap it in an <a> tag

  if (typeof children === 'string') {
    children = /*#__PURE__*/_react.default.createElement("a", null, children);
  } // This will return the first child, if multiple are provided it will throw an error


  const child = _react.Children.only(children);

  const childRef = child && typeof child === 'object' && child.ref;
  const [setIntersectionRef, isVisible] = (0, _useIntersection.useIntersection)({
    rootMargin: '200px'
  });

  const setRef = _react.default.useCallback(el => {
    setIntersectionRef(el);

    if (childRef) {
      if (typeof childRef === 'function') childRef(el);else if (typeof childRef === 'object') {
        childRef.current = el;
      }
    }
  }, [childRef, setIntersectionRef]);

  (0, _react.useEffect)(() => {
    const shouldPrefetch = isVisible && p && (0, _router.isLocalURL)(href);
    const curLocale = typeof locale !== 'undefined' ? locale : router && router.locale;
    const isPrefetched = prefetched[href + '%' + as + (curLocale ? '%' + curLocale : '')];

    if (shouldPrefetch && !isPrefetched) {
      prefetch(router, href, as, {
        locale: curLocale
      });
    }
  }, [as, href, isVisible, locale, p, router]);
  const childProps = {
    ref: setRef,
    onClick: e => {
      if (child.props && typeof child.props.onClick === 'function') {
        child.props.onClick(e);
      }

      if (!e.defaultPrevented) {
        linkClicked(e, router, href, as, replace, shallow, scroll, locale);
      }
    }
  };

  childProps.onMouseEnter = e => {
    if (!(0, _router.isLocalURL)(href)) return;

    if (child.props && typeof child.props.onMouseEnter === 'function') {
      child.props.onMouseEnter(e);
    }

    prefetch(router, href, as, {
      priority: true
    });
  }; // If child is an <a> tag and doesn't have a href attribute, or if the 'passHref' property is
  // defined, we specify the current 'href', so that repetition is not needed by the user


  if (props.passHref || child.type === 'a' && !('href' in child.props)) {
    const curLocale = typeof locale !== 'undefined' ? locale : router && router.locale;
    const localeDomain = (0, _router.getDomainLocale)(as, curLocale, router && router.locales, router && router.domainLocales);
    childProps.href = localeDomain || (0, _router.addBasePath)((0, _router.addLocale)(as, curLocale, router && router.defaultLocale));
  }

  return /*#__PURE__*/_react.default.cloneElement(child, childProps);
}

var _default = Link;
exports.default = _default;

/***/ }),

/***/ 6528:
/***/ (function(__unused_webpack_module, exports) {

"use strict";


exports.__esModule = true;
exports.removePathTrailingSlash = removePathTrailingSlash;
exports.normalizePathTrailingSlash = void 0;
/**
* Removes the trailing slash of a path if there is one. Preserves the root path `/`.
*/

function removePathTrailingSlash(path) {
  return path.endsWith('/') && path !== '/' ? path.slice(0, -1) : path;
}
/**
* Normalizes the trailing slash of a path according to the `trailingSlash` option
* in `next.config.js`.
*/


const normalizePathTrailingSlash =  false ? 0 : removePathTrailingSlash;
exports.normalizePathTrailingSlash = normalizePathTrailingSlash;

/***/ }),

/***/ 8391:
/***/ (function(__unused_webpack_module, exports) {

"use strict";


exports.__esModule = true;
exports.default = void 0;

const requestIdleCallback = typeof self !== 'undefined' && self.requestIdleCallback || function (cb) {
  let start = Date.now();
  return setTimeout(function () {
    cb({
      didTimeout: false,
      timeRemaining: function () {
        return Math.max(0, 50 - (Date.now() - start));
      }
    });
  }, 1);
};

var _default = requestIdleCallback;
exports.default = _default;

/***/ }),

/***/ 7599:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(5318);

exports.__esModule = true;
exports.markAssetError = markAssetError;
exports.isAssetError = isAssetError;
exports.getClientBuildManifest = getClientBuildManifest;
exports.default = void 0;

var _getAssetPathFromRoute = _interopRequireDefault(__webpack_require__(2238));

var _requestIdleCallback = _interopRequireDefault(__webpack_require__(8391)); // 3.8s was arbitrarily chosen as it's what https://web.dev/interactive
// considers as "Good" time-to-interactive. We must assume something went
// wrong beyond this point, and then fall-back to a full page transition to
// show the user something of value.


const MS_MAX_IDLE_DELAY = 3800;

function withFuture(key, map, generator) {
  let entry = map.get(key);

  if (entry) {
    if ('future' in entry) {
      return entry.future;
    }

    return Promise.resolve(entry);
  }

  let resolver;
  const prom = new Promise(resolve => {
    resolver = resolve;
  });
  map.set(key, entry = {
    resolve: resolver,
    future: prom
  });
  return generator ? // eslint-disable-next-line no-sequences
  generator().then(value => (resolver(value), value)) : prom;
}

function hasPrefetch(link) {
  try {
    link = document.createElement('link');
    return (// detect IE11 since it supports prefetch but isn't detected
      // with relList.support
      !!window.MSInputMethodContext && !!document.documentMode || link.relList.supports('prefetch')
    );
  } catch (_unused) {
    return false;
  }
}

const canPrefetch = hasPrefetch();

function prefetchViaDom(href, as, link) {
  return new Promise((res, rej) => {
    if (document.querySelector(`link[rel="prefetch"][href^="${href}"]`)) {
      return res();
    }

    link = document.createElement('link'); // The order of property assignment here is intentional:

    if (as) link.as = as;
    link.rel = `prefetch`;
    link.crossOrigin = undefined;
    link.onload = res;
    link.onerror = rej; // `href` should always be last:

    link.href = href;
    document.head.appendChild(link);
  });
}

const ASSET_LOAD_ERROR = Symbol('ASSET_LOAD_ERROR'); // TODO: unexport

function markAssetError(err) {
  return Object.defineProperty(err, ASSET_LOAD_ERROR, {});
}

function isAssetError(err) {
  return err && ASSET_LOAD_ERROR in err;
}

function appendScript(src, script) {
  return new Promise((resolve, reject) => {
    script = document.createElement('script'); // The order of property assignment here is intentional.
    // 1. Setup success/failure hooks in case the browser synchronously
    //    executes when `src` is set.

    script.onload = resolve;

    script.onerror = () => reject(markAssetError(new Error(`Failed to load script: ${src}`))); // 2. Configure the cross-origin attribute before setting `src` in case the
    //    browser begins to fetch.


    script.crossOrigin = undefined; // 3. Finally, set the source and inject into the DOM in case the child
    //    must be appended for fetching to start.

    script.src = src;
    document.body.appendChild(script);
  });
}

function idleTimeout(ms, err) {
  return new Promise((_resolve, reject) => (0, _requestIdleCallback.default)(() => setTimeout(() => reject(err), ms)));
} // TODO: stop exporting or cache the failure
// It'd be best to stop exporting this. It's an implementation detail. We're
// only exporting it for backwards compatibilty with the `page-loader`.
// Only cache this response as a last resort if we cannot eliminate all other
// code branches that use the Build Manifest Callback and push them through
// the Route Loader interface.


function getClientBuildManifest() {
  if (self.__BUILD_MANIFEST) {
    return Promise.resolve(self.__BUILD_MANIFEST);
  }

  const onBuildManifest = new Promise(resolve => {
    // Mandatory because this is not concurrent safe:
    const cb = self.__BUILD_MANIFEST_CB;

    self.__BUILD_MANIFEST_CB = () => {
      resolve(self.__BUILD_MANIFEST);
      cb && cb();
    };
  });
  return Promise.race([onBuildManifest, idleTimeout(MS_MAX_IDLE_DELAY, markAssetError(new Error('Failed to load client build manifest')))]);
}

function getFilesForRoute(assetPrefix, route) {
  if (false) {}

  return getClientBuildManifest().then(manifest => {
    if (!(route in manifest)) {
      throw markAssetError(new Error(`Failed to lookup route: ${route}`));
    }

    const allFiles = manifest[route].map(entry => assetPrefix + '/_next/' + encodeURI(entry));
    return {
      scripts: allFiles.filter(v => v.endsWith('.js')),
      css: allFiles.filter(v => v.endsWith('.css'))
    };
  });
}

function createRouteLoader(assetPrefix) {
  const entrypoints = new Map();
  const loadedScripts = new Map();
  const styleSheets = new Map();
  const routes = new Map();

  function maybeExecuteScript(src) {
    let prom = loadedScripts.get(src);

    if (prom) {
      return prom;
    } // Skip executing script if it's already in the DOM:


    if (document.querySelector(`script[src^="${src}"]`)) {
      return Promise.resolve();
    }

    loadedScripts.set(src, prom = appendScript(src));
    return prom;
  }

  function fetchStyleSheet(href) {
    let prom = styleSheets.get(href);

    if (prom) {
      return prom;
    }

    styleSheets.set(href, prom = fetch(href).then(res => {
      if (!res.ok) {
        throw new Error(`Failed to load stylesheet: ${href}`);
      }

      return res.text().then(text => ({
        href: href,
        content: text
      }));
    }).catch(err => {
      throw markAssetError(err);
    }));
    return prom;
  }

  return {
    whenEntrypoint(route) {
      return withFuture(route, entrypoints);
    },

    onEntrypoint(route, execute) {
      Promise.resolve(execute).then(fn => fn()).then(exports => ({
        component: exports && exports.default || exports,
        exports: exports
      }), err => ({
        error: err
      })).then(input => {
        const old = entrypoints.get(route);
        entrypoints.set(route, input);
        if (old && 'resolve' in old) old.resolve(input);
      });
    },

    loadRoute(route) {
      return withFuture(route, routes, async () => {
        try {
          const {
            scripts,
            css
          } = await getFilesForRoute(assetPrefix, route);
          const [, styles] = await Promise.all([entrypoints.has(route) ? [] : Promise.all(scripts.map(maybeExecuteScript)), Promise.all(css.map(fetchStyleSheet))]);
          const entrypoint = await Promise.race([this.whenEntrypoint(route), idleTimeout(MS_MAX_IDLE_DELAY, markAssetError(new Error(`Route did not complete loading: ${route}`)))]);
          const res = Object.assign({
            styles
          }, entrypoint);
          return 'error' in entrypoint ? entrypoint : res;
        } catch (err) {
          return {
            error: err
          };
        }
      });
    },

    prefetch(route) {
      // https://github.com/GoogleChromeLabs/quicklink/blob/453a661fa1fa940e2d2e044452398e38c67a98fb/src/index.mjs#L115-L118
      // License: Apache 2.0
      let cn;

      if (cn = navigator.connection) {
        // Don't prefetch if using 2G or if Save-Data is enabled.
        if (cn.saveData || /2g/.test(cn.effectiveType)) return Promise.resolve();
      }

      return getFilesForRoute(assetPrefix, route).then(output => Promise.all(canPrefetch ? output.scripts.map(script => prefetchViaDom(script, 'script')) : [])).then(() => {
        (0, _requestIdleCallback.default)(() => this.loadRoute(route));
      }).catch( // swallow prefetch errors
      () => {});
    }

  };
}

var _default = createRouteLoader;
exports.default = _default;

/***/ }),

/***/ 2441:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(862);

var _interopRequireDefault = __webpack_require__(5318);

exports.__esModule = true;
exports.useRouter = useRouter;
exports.makePublicRouterInstance = makePublicRouterInstance;
exports.createRouter = exports.withRouter = exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__(9297));

var _router2 = _interopRequireWildcard(__webpack_require__(1689));

exports.Router = _router2.default;
exports.NextRouter = _router2.NextRouter;

var _routerContext = __webpack_require__(8417);

var _withRouter = _interopRequireDefault(__webpack_require__(3168));

exports.withRouter = _withRouter.default;
/* global window */

const singletonRouter = {
  router: null,
  // holds the actual router instance
  readyCallbacks: [],

  ready(cb) {
    if (this.router) return cb();

    if (false) {}
  }

}; // Create public properties and methods of the router in the singletonRouter

const urlPropertyFields = ['pathname', 'route', 'query', 'asPath', 'components', 'isFallback', 'basePath', 'locale', 'locales', 'defaultLocale', 'isReady'];
const routerEvents = ['routeChangeStart', 'beforeHistoryChange', 'routeChangeComplete', 'routeChangeError', 'hashChangeStart', 'hashChangeComplete'];
const coreMethodFields = ['push', 'replace', 'reload', 'back', 'prefetch', 'beforePopState']; // Events is a static property on the router, the router doesn't have to be initialized to use it

Object.defineProperty(singletonRouter, 'events', {
  get() {
    return _router2.default.events;
  }

});
urlPropertyFields.forEach(field => {
  // Here we need to use Object.defineProperty because, we need to return
  // the property assigned to the actual router
  // The value might get changed as we change routes and this is the
  // proper way to access it
  Object.defineProperty(singletonRouter, field, {
    get() {
      const router = getRouter();
      return router[field];
    }

  });
});
coreMethodFields.forEach(field => {
  // We don't really know the types here, so we add them later instead
  ;

  singletonRouter[field] = (...args) => {
    const router = getRouter();
    return router[field](...args);
  };
});
routerEvents.forEach(event => {
  singletonRouter.ready(() => {
    _router2.default.events.on(event, (...args) => {
      const eventField = `on${event.charAt(0).toUpperCase()}${event.substring(1)}`;
      const _singletonRouter = singletonRouter;

      if (_singletonRouter[eventField]) {
        try {
          _singletonRouter[eventField](...args);
        } catch (err) {
          console.error(`Error when running the Router event: ${eventField}`);
          console.error(`${err.message}\n${err.stack}`);
        }
      }
    });
  });
});

function getRouter() {
  if (!singletonRouter.router) {
    const message = 'No router instance found.\n' + 'You should only use "next/router" inside the client side of your app.\n';
    throw new Error(message);
  }

  return singletonRouter.router;
} // Export the singletonRouter and this is the public API.


var _default = singletonRouter; // Reexport the withRoute HOC

exports.default = _default;

function useRouter() {
  return _react.default.useContext(_routerContext.RouterContext);
} // INTERNAL APIS
// -------------
// (do not use following exports inside the app)
// Create a router and assign it as the singleton instance.
// This is used in client side when we are initilizing the app.
// This should **not** use inside the server.


const createRouter = (...args) => {
  singletonRouter.router = new _router2.default(...args);
  singletonRouter.readyCallbacks.forEach(cb => cb());
  singletonRouter.readyCallbacks = [];
  return singletonRouter.router;
}; // This function is used to create the `withRouter` router instance


exports.createRouter = createRouter;

function makePublicRouterInstance(router) {
  const _router = router;
  const instance = {};

  for (const property of urlPropertyFields) {
    if (typeof _router[property] === 'object') {
      instance[property] = Object.assign(Array.isArray(_router[property]) ? [] : {}, _router[property]); // makes sure query is not stateful

      continue;
    }

    instance[property] = _router[property];
  } // Events is a static property on the router, the router doesn't have to be initialized to use it


  instance.events = _router2.default.events;
  coreMethodFields.forEach(field => {
    instance[field] = (...args) => {
      return _router[field](...args);
    };
  });
  return instance;
}

/***/ }),

/***/ 5749:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(5318);

exports.__esModule = true;
exports.useIntersection = useIntersection;

var _react = __webpack_require__(9297);

var _requestIdleCallback = _interopRequireDefault(__webpack_require__(8391));

const hasIntersectionObserver = typeof IntersectionObserver !== 'undefined';

function useIntersection({
  rootMargin,
  disabled
}) {
  const isDisabled = disabled || !hasIntersectionObserver;
  const unobserve = (0, _react.useRef)();
  const [visible, setVisible] = (0, _react.useState)(false);
  const setRef = (0, _react.useCallback)(el => {
    if (unobserve.current) {
      unobserve.current();
      unobserve.current = undefined;
    }

    if (isDisabled || visible) return;

    if (el && el.tagName) {
      unobserve.current = observe(el, isVisible => isVisible && setVisible(isVisible), {
        rootMargin
      });
    }
  }, [isDisabled, rootMargin, visible]);
  (0, _react.useEffect)(() => {
    if (!hasIntersectionObserver) {
      if (!visible) (0, _requestIdleCallback.default)(() => setVisible(true));
    }
  }, [visible]);
  return [setRef, visible];
}

function observe(element, callback, options) {
  const {
    id,
    observer,
    elements
  } = createObserver(options);
  elements.set(element, callback);
  observer.observe(element);
  return function unobserve() {
    elements.delete(element);
    observer.unobserve(element); // Destroy observer when there's nothing left to watch:

    if (elements.size === 0) {
      observer.disconnect();
      observers.delete(id);
    }
  };
}

const observers = new Map();

function createObserver(options) {
  const id = options.rootMargin || '';
  let instance = observers.get(id);

  if (instance) {
    return instance;
  }

  const elements = new Map();
  const observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      const callback = elements.get(entry.target);
      const isVisible = entry.isIntersecting || entry.intersectionRatio > 0;

      if (callback && isVisible) {
        callback(isVisible);
      }
    });
  }, options);
  observers.set(id, instance = {
    id,
    observer,
    elements
  });
  return instance;
}

/***/ }),

/***/ 3168:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(5318);

exports.__esModule = true;
exports.default = withRouter;

var _react = _interopRequireDefault(__webpack_require__(9297));

var _router = __webpack_require__(2441);

function withRouter(ComposedComponent) {
  function WithRouterWrapper(props) {
    return /*#__PURE__*/_react.default.createElement(ComposedComponent, Object.assign({
      router: (0, _router.useRouter)()
    }, props));
  }

  WithRouterWrapper.getInitialProps = ComposedComponent.getInitialProps // This is needed to allow checking for custom getInitialProps in _app
  ;
  WithRouterWrapper.origGetInitialProps = ComposedComponent.origGetInitialProps;

  if (false) {}

  return WithRouterWrapper;
}

/***/ }),

/***/ 1253:
/***/ (function(__unused_webpack_module, exports) {

"use strict";


exports.__esModule = true;
exports.normalizeLocalePath = normalizeLocalePath;

function normalizeLocalePath(pathname, locales) {
  let detectedLocale; // first item will be empty string from splitting at first char

  const pathnameParts = pathname.split('/');
  (locales || []).some(locale => {
    if (pathnameParts[1].toLowerCase() === locale.toLowerCase()) {
      detectedLocale = locale;
      pathnameParts.splice(1, 1);
      pathname = pathnameParts.join('/') || '/';
      return true;
    }

    return false;
  });
  return {
    pathname,
    detectedLocale
  };
}

/***/ }),

/***/ 7332:
/***/ (function(__unused_webpack_module, exports) {

"use strict";


exports.__esModule = true;
exports.default = mitt;
/*
MIT License
Copyright (c) Jason Miller (https://jasonformat.com/)
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
// This file is based on https://github.com/developit/mitt/blob/v1.1.3/src/index.js
// It's been edited for the needs of this script
// See the LICENSE at the top of the file

function mitt() {
  const all = Object.create(null);
  return {
    on(type, handler) {
      ;
      (all[type] || (all[type] = [])).push(handler);
    },

    off(type, handler) {
      if (all[type]) {
        all[type].splice(all[type].indexOf(handler) >>> 0, 1);
      }
    },

    emit(type, ...evts) {
      // eslint-disable-next-line array-callback-return
      ;
      (all[type] || []).slice().map(handler => {
        handler(...evts);
      });
    }

  };
}

/***/ }),

/***/ 1689:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.getDomainLocale = getDomainLocale;
exports.addLocale = addLocale;
exports.delLocale = delLocale;
exports.hasBasePath = hasBasePath;
exports.addBasePath = addBasePath;
exports.delBasePath = delBasePath;
exports.isLocalURL = isLocalURL;
exports.interpolateAs = interpolateAs;
exports.resolveHref = resolveHref;
exports.default = void 0;

var _normalizeTrailingSlash = __webpack_require__(6528);

var _routeLoader = __webpack_require__(7599);

var _denormalizePagePath = __webpack_require__(9320);

var _normalizeLocalePath = __webpack_require__(1253);

var _mitt = _interopRequireDefault(__webpack_require__(7332));

var _utils = __webpack_require__(3937);

var _isDynamic = __webpack_require__(3288);

var _parseRelativeUrl = __webpack_require__(4436);

var _querystring = __webpack_require__(4915);

var _resolveRewrites = _interopRequireDefault(__webpack_require__(8535));

var _routeMatcher = __webpack_require__(7451);

var _routeRegex = __webpack_require__(8193);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
}
/* global __NEXT_DATA__ */
// tslint:disable:no-console


let detectDomainLocale;

if (false) {}

const basePath =  false || '';

function buildCancellationError() {
  return Object.assign(new Error('Route Cancelled'), {
    cancelled: true
  });
}

function addPathPrefix(path, prefix) {
  return prefix && path.startsWith('/') ? path === '/' ? (0, _normalizeTrailingSlash.normalizePathTrailingSlash)(prefix) : `${prefix}${pathNoQueryHash(path) === '/' ? path.substring(1) : path}` : path;
}

function getDomainLocale(path, locale, locales, domainLocales) {
  if (false) {}

  return false;
}

function addLocale(path, locale, defaultLocale) {
  if (false) {}

  return path;
}

function delLocale(path, locale) {
  if (false) {}

  return path;
}

function pathNoQueryHash(path) {
  const queryIndex = path.indexOf('?');
  const hashIndex = path.indexOf('#');

  if (queryIndex > -1 || hashIndex > -1) {
    path = path.substring(0, queryIndex > -1 ? queryIndex : hashIndex);
  }

  return path;
}

function hasBasePath(path) {
  path = pathNoQueryHash(path);
  return path === basePath || path.startsWith(basePath + '/');
}

function addBasePath(path) {
  // we only add the basepath on relative urls
  return addPathPrefix(path, basePath);
}

function delBasePath(path) {
  path = path.slice(basePath.length);
  if (!path.startsWith('/')) path = `/${path}`;
  return path;
}
/**
* Detects whether a given url is routable by the Next.js router (browser only).
*/


function isLocalURL(url) {
  if (url.startsWith('/')) return true;

  try {
    // absolute urls can be local if they are on the same origin
    const locationOrigin = (0, _utils.getLocationOrigin)();
    const resolved = new URL(url, locationOrigin);
    return resolved.origin === locationOrigin && hasBasePath(resolved.pathname);
  } catch (_) {
    return false;
  }
}

function interpolateAs(route, asPathname, query) {
  let interpolatedRoute = '';
  const dynamicRegex = (0, _routeRegex.getRouteRegex)(route);
  const dynamicGroups = dynamicRegex.groups;
  const dynamicMatches = // Try to match the dynamic route against the asPath
  (asPathname !== route ? (0, _routeMatcher.getRouteMatcher)(dynamicRegex)(asPathname) : '') || // Fall back to reading the values from the href
  // TODO: should this take priority; also need to change in the router.
  query;
  interpolatedRoute = route;
  const params = Object.keys(dynamicGroups);

  if (!params.every(param => {
    let value = dynamicMatches[param] || '';
    const {
      repeat,
      optional
    } = dynamicGroups[param]; // support single-level catch-all
    // TODO: more robust handling for user-error (passing `/`)

    let replaced = `[${repeat ? '...' : ''}${param}]`;

    if (optional) {
      replaced = `${!value ? '/' : ''}[${replaced}]`;
    }

    if (repeat && !Array.isArray(value)) value = [value];
    return (optional || param in dynamicMatches) && ( // Interpolate group into data URL if present
    interpolatedRoute = interpolatedRoute.replace(replaced, repeat ? value.map( // these values should be fully encoded instead of just
    // path delimiter escaped since they are being inserted
    // into the URL and we expect URL encoded segments
    // when parsing dynamic route params
    segment => encodeURIComponent(segment)).join('/') : encodeURIComponent(value)) || '/');
  })) {
    interpolatedRoute = ''; // did not satisfy all requirements
    // n.b. We ignore this error because we handle warning for this case in
    // development in the `<Link>` component directly.
  }

  return {
    params,
    result: interpolatedRoute
  };
}

function omitParmsFromQuery(query, params) {
  const filteredQuery = {};
  Object.keys(query).forEach(key => {
    if (!params.includes(key)) {
      filteredQuery[key] = query[key];
    }
  });
  return filteredQuery;
}
/**
* Resolves a given hyperlink with a certain router state (basePath not included).
* Preserves absolute urls.
*/


function resolveHref(currentPath, href, resolveAs) {
  // we use a dummy base url for relative urls
  const base = new URL(currentPath, 'http://n');
  const urlAsString = typeof href === 'string' ? href : (0, _utils.formatWithValidation)(href); // Return because it cannot be routed by the Next.js router

  if (!isLocalURL(urlAsString)) {
    return resolveAs ? [urlAsString] : urlAsString;
  }

  try {
    const finalUrl = new URL(urlAsString, base);
    finalUrl.pathname = (0, _normalizeTrailingSlash.normalizePathTrailingSlash)(finalUrl.pathname);
    let interpolatedAs = '';

    if ((0, _isDynamic.isDynamicRoute)(finalUrl.pathname) && finalUrl.searchParams && resolveAs) {
      const query = (0, _querystring.searchParamsToUrlQuery)(finalUrl.searchParams);
      const {
        result,
        params
      } = interpolateAs(finalUrl.pathname, finalUrl.pathname, query);

      if (result) {
        interpolatedAs = (0, _utils.formatWithValidation)({
          pathname: result,
          hash: finalUrl.hash,
          query: omitParmsFromQuery(query, params)
        });
      }
    } // if the origin didn't change, it means we received a relative href


    const resolvedHref = finalUrl.origin === base.origin ? finalUrl.href.slice(finalUrl.origin.length) : finalUrl.href;
    return resolveAs ? [resolvedHref, interpolatedAs || resolvedHref] : resolvedHref;
  } catch (_) {
    return resolveAs ? [urlAsString] : urlAsString;
  }
}

function stripOrigin(url) {
  const origin = (0, _utils.getLocationOrigin)();
  return url.startsWith(origin) ? url.substring(origin.length) : url;
}

function prepareUrlAs(router, url, as) {
  // If url and as provided as an object representation,
  // we'll format them into the string version here.
  let [resolvedHref, resolvedAs] = resolveHref(router.pathname, url, true);
  const origin = (0, _utils.getLocationOrigin)();
  const hrefHadOrigin = resolvedHref.startsWith(origin);
  const asHadOrigin = resolvedAs && resolvedAs.startsWith(origin);
  resolvedHref = stripOrigin(resolvedHref);
  resolvedAs = resolvedAs ? stripOrigin(resolvedAs) : resolvedAs;
  const preparedUrl = hrefHadOrigin ? resolvedHref : addBasePath(resolvedHref);
  const preparedAs = as ? stripOrigin(resolveHref(router.pathname, as)) : resolvedAs || resolvedHref;
  return {
    url: preparedUrl,
    as: asHadOrigin ? preparedAs : addBasePath(preparedAs)
  };
}

const manualScrollRestoration = (/* unused pure expression or super */ null && ( false && 0));
const SSG_DATA_NOT_FOUND = Symbol('SSG_DATA_NOT_FOUND');

function fetchRetry(url, attempts) {
  return fetch(url, {
    // Cookies are required to be present for Next.js' SSG "Preview Mode".
    // Cookies may also be required for `getServerSideProps`.
    //
    // > `fetch` won’t send cookies, unless you set the credentials init
    // > option.
    // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
    //
    // > For maximum browser compatibility when it comes to sending &
    // > receiving cookies, always supply the `credentials: 'same-origin'`
    // > option instead of relying on the default.
    // https://github.com/github/fetch#caveats
    credentials: 'same-origin'
  }).then(res => {
    if (!res.ok) {
      if (attempts > 1 && res.status >= 500) {
        return fetchRetry(url, attempts - 1);
      }

      if (res.status === 404) {
        return res.json().then(data => {
          if (data.notFound) {
            return {
              notFound: SSG_DATA_NOT_FOUND
            };
          }

          throw new Error(`Failed to load static props`);
        });
      }

      throw new Error(`Failed to load static props`);
    }

    return res.json();
  });
}

function fetchNextData(dataHref, isServerRender) {
  return fetchRetry(dataHref, isServerRender ? 3 : 1).catch(err => {
    // We should only trigger a server-side transition if this was caused
    // on a client-side transition. Otherwise, we'd get into an infinite
    // loop.
    if (!isServerRender) {
      (0, _routeLoader.markAssetError)(err);
    }

    throw err;
  });
}

class Router {
  /**
  * Map of all components loaded in `Router`
  */
  // Static Data Cache
  constructor(_pathname, _query, _as, {
    initialProps,
    pageLoader,
    App,
    wrapApp,
    Component,
    err,
    subscription,
    isFallback,
    locale,
    locales,
    defaultLocale,
    domainLocales
  }) {
    this.route = void 0;
    this.pathname = void 0;
    this.query = void 0;
    this.asPath = void 0;
    this.basePath = void 0;
    this.components = void 0;
    this.sdc = {};
    this.sub = void 0;
    this.clc = void 0;
    this.pageLoader = void 0;
    this._bps = void 0;
    this.events = void 0;
    this._wrapApp = void 0;
    this.isSsr = void 0;
    this.isFallback = void 0;
    this._inFlightRoute = void 0;
    this._shallow = void 0;
    this.locale = void 0;
    this.locales = void 0;
    this.defaultLocale = void 0;
    this.domainLocales = void 0;
    this.isReady = void 0;
    this._idx = 0;

    this.onPopState = e => {
      const state = e.state;

      if (!state) {
        // We get state as undefined for two reasons.
        //  1. With older safari (< 8) and older chrome (< 34)
        //  2. When the URL changed with #
        //
        // In the both cases, we don't need to proceed and change the route.
        // (as it's already changed)
        // But we can simply replace the state with the new changes.
        // Actually, for (1) we don't need to nothing. But it's hard to detect that event.
        // So, doing the following for (1) does no harm.
        const {
          pathname,
          query
        } = this;
        this.changeState('replaceState', (0, _utils.formatWithValidation)({
          pathname: addBasePath(pathname),
          query
        }), (0, _utils.getURL)());
        return;
      }

      if (!state.__N) {
        return;
      }

      let forcedScroll;
      const {
        url,
        as,
        options,
        idx
      } = state;

      if (false) {}

      this._idx = idx;
      const {
        pathname
      } = (0, _parseRelativeUrl.parseRelativeUrl)(url); // Make sure we don't re-render on initial load,
      // can be caused by navigating back from an external site

      if (this.isSsr && as === this.asPath && pathname === this.pathname) {
        return;
      } // If the downstream application returns falsy, return.
      // They will then be responsible for handling the event.


      if (this._bps && !this._bps(state)) {
        return;
      }

      this.change('replaceState', url, as, Object.assign({}, options, {
        shallow: options.shallow && this._shallow,
        locale: options.locale || this.defaultLocale
      }), forcedScroll);
    }; // represents the current component key


    this.route = (0, _normalizeTrailingSlash.removePathTrailingSlash)(_pathname); // set up the component cache (by route keys)

    this.components = {}; // We should not keep the cache, if there's an error
    // Otherwise, this cause issues when when going back and
    // come again to the errored page.

    if (_pathname !== '/_error') {
      this.components[this.route] = {
        Component,
        initial: true,
        props: initialProps,
        err,
        __N_SSG: initialProps && initialProps.__N_SSG,
        __N_SSP: initialProps && initialProps.__N_SSP
      };
    }

    this.components['/_app'] = {
      Component: App,
      styleSheets: [
        /* /_app does not need its stylesheets managed */
      ]
    }; // Backwards compat for Router.router.events
    // TODO: Should be remove the following major version as it was never documented

    this.events = Router.events;
    this.pageLoader = pageLoader;
    this.pathname = _pathname;
    this.query = _query; // if auto prerendered and dynamic route wait to update asPath
    // until after mount to prevent hydration mismatch

    const autoExportDynamic = (0, _isDynamic.isDynamicRoute)(_pathname) && self.__NEXT_DATA__.autoExport;

    this.asPath = autoExportDynamic ? _pathname : _as;
    this.basePath = basePath;
    this.sub = subscription;
    this.clc = null;
    this._wrapApp = wrapApp; // make sure to ignore extra popState in safari on navigating
    // back from external site

    this.isSsr = true;
    this.isFallback = isFallback;
    this.isReady = !!(self.__NEXT_DATA__.gssp || self.__NEXT_DATA__.gip || !autoExportDynamic && !self.location.search);

    if (false) {}

    if (false) {}
  }

  reload() {
    window.location.reload();
  }
  /**
  * Go back in history
  */


  back() {
    window.history.back();
  }
  /**
  * Performs a `pushState` with arguments
  * @param url of the route
  * @param as masks `url` for the browser
  * @param options object you can define `shallow` and other options
  */


  push(url, as, options = {}) {
    if (false) {}

    ;
    ({
      url,
      as
    } = prepareUrlAs(this, url, as));
    return this.change('pushState', url, as, options);
  }
  /**
  * Performs a `replaceState` with arguments
  * @param url of the route
  * @param as masks `url` for the browser
  * @param options object you can define `shallow` and other options
  */


  replace(url, as, options = {}) {
    ;
    ({
      url,
      as
    } = prepareUrlAs(this, url, as));
    return this.change('replaceState', url, as, options);
  }

  async change(method, url, as, options, forcedScroll) {
    var _options$scroll;

    if (!isLocalURL(url)) {
      window.location.href = url;
      return false;
    } // for static pages with query params in the URL we delay
    // marking the router ready until after the query is updated


    if (options._h) {
      this.isReady = true;
    } // Default to scroll reset behavior unless explicitly specified to be
    // `false`! This makes the behavior between using `Router#push` and a
    // `<Link />` consistent.


    options.scroll = !!((_options$scroll = options.scroll) != null ? _options$scroll : true);
    let localeChange = options.locale !== this.locale;

    if (false) { var _this$locales; }

    if (!options._h) {
      this.isSsr = false;
    } // marking route changes as a navigation start entry


    if (_utils.ST) {
      performance.mark('routeChange');
    }

    const {
      shallow = false
    } = options;
    const routeProps = {
      shallow
    };

    if (this._inFlightRoute) {
      this.abortComponentLoad(this._inFlightRoute, routeProps);
    }

    as = addBasePath(addLocale(hasBasePath(as) ? delBasePath(as) : as, options.locale, this.defaultLocale));
    const cleanedAs = delLocale(hasBasePath(as) ? delBasePath(as) : as, this.locale);
    this._inFlightRoute = as; // If the url change is only related to a hash change
    // We should not proceed. We should only change the state.
    // WARNING: `_h` is an internal option for handing Next.js client-side
    // hydration. Your app should _never_ use this property. It may change at
    // any time without notice.

    if (!options._h && this.onlyAHashChange(cleanedAs)) {
      this.asPath = cleanedAs;
      Router.events.emit('hashChangeStart', as, routeProps); // TODO: do we need the resolved href when only a hash change?

      this.changeState(method, url, as, options);
      this.scrollToHash(cleanedAs);
      this.notify(this.components[this.route], null);
      Router.events.emit('hashChangeComplete', as, routeProps);
      return true;
    }

    let parsed = (0, _parseRelativeUrl.parseRelativeUrl)(url);
    let {
      pathname,
      query
    } = parsed; // The build manifest needs to be loaded before auto-static dynamic pages
    // get their query parameters to allow ensuring they can be parsed properly
    // when rewritten to

    let pages, rewrites;

    try {
      pages = await this.pageLoader.getPageList();
      ({
        __rewrites: rewrites
      } = await (0, _routeLoader.getClientBuildManifest)());
    } catch (err) {
      // If we fail to resolve the page list or client-build manifest, we must
      // do a server-side transition:
      window.location.href = as;
      return false;
    }

    parsed = this._resolveHref(parsed, pages);

    if (parsed.pathname !== pathname) {
      pathname = parsed.pathname;
      url = (0, _utils.formatWithValidation)(parsed);
    } // url and as should always be prefixed with basePath by this
    // point by either next/link or router.push/replace so strip the
    // basePath from the pathname to match the pages dir 1-to-1


    pathname = pathname ? (0, _normalizeTrailingSlash.removePathTrailingSlash)(delBasePath(pathname)) : pathname; // If asked to change the current URL we should reload the current page
    // (not location.reload() but reload getInitialProps and other Next.js stuffs)
    // We also need to set the method = replaceState always
    // as this should not go into the history (That's how browsers work)
    // We should compare the new asPath to the current asPath, not the url

    if (!this.urlIsNew(cleanedAs) && !localeChange) {
      method = 'replaceState';
    }

    let route = (0, _normalizeTrailingSlash.removePathTrailingSlash)(pathname); // we need to resolve the as value using rewrites for dynamic SSG
    // pages to allow building the data URL correctly

    let resolvedAs = as;

    if (false) {}

    if (!isLocalURL(as)) {
      if (false) {}

      window.location.href = as;
      return false;
    }

    resolvedAs = delLocale(delBasePath(resolvedAs), this.locale);

    if ((0, _isDynamic.isDynamicRoute)(route)) {
      const parsedAs = (0, _parseRelativeUrl.parseRelativeUrl)(resolvedAs);
      const asPathname = parsedAs.pathname;
      const routeRegex = (0, _routeRegex.getRouteRegex)(route);
      const routeMatch = (0, _routeMatcher.getRouteMatcher)(routeRegex)(asPathname);
      const shouldInterpolate = route === asPathname;
      const interpolatedAs = shouldInterpolate ? interpolateAs(route, asPathname, query) : {};

      if (!routeMatch || shouldInterpolate && !interpolatedAs.result) {
        const missingParams = Object.keys(routeRegex.groups).filter(param => !query[param]);

        if (missingParams.length > 0) {
          if (false) {}

          throw new Error((shouldInterpolate ? `The provided \`href\` (${url}) value is missing query values (${missingParams.join(', ')}) to be interpolated properly. ` : `The provided \`as\` value (${asPathname}) is incompatible with the \`href\` value (${route}). `) + `Read more: https://err.sh/vercel/next.js/${shouldInterpolate ? 'href-interpolation-failed' : 'incompatible-href-as'}`);
        }
      } else if (shouldInterpolate) {
        as = (0, _utils.formatWithValidation)(Object.assign({}, parsedAs, {
          pathname: interpolatedAs.result,
          query: omitParmsFromQuery(query, interpolatedAs.params)
        }));
      } else {
        // Merge params into `query`, overwriting any specified in search
        Object.assign(query, routeMatch);
      }
    }

    Router.events.emit('routeChangeStart', as, routeProps);

    try {
      let routeInfo = await this.getRouteInfo(route, pathname, query, addBasePath(addLocale(resolvedAs, this.locale)), routeProps);
      let {
        error,
        props,
        __N_SSG,
        __N_SSP
      } = routeInfo; // handle redirect on client-transition

      if ((__N_SSG || __N_SSP) && props) {
        if (props.pageProps && props.pageProps.__N_REDIRECT) {
          const destination = props.pageProps.__N_REDIRECT; // check if destination is internal (resolves to a page) and attempt
          // client-navigation if it is falling back to hard navigation if
          // it's not

          if (destination.startsWith('/')) {
            const parsedHref = (0, _parseRelativeUrl.parseRelativeUrl)(destination);

            this._resolveHref(parsedHref, pages, false);

            if (pages.includes(parsedHref.pathname)) {
              const {
                url: newUrl,
                as: newAs
              } = prepareUrlAs(this, destination, destination);
              return this.change(method, newUrl, newAs, options);
            }
          }

          window.location.href = destination;
          return new Promise(() => {});
        } // handle SSG data 404


        if (props.notFound === SSG_DATA_NOT_FOUND) {
          let notFoundRoute;

          try {
            await this.fetchComponent('/404');
            notFoundRoute = '/404';
          } catch (_) {
            notFoundRoute = '/_error';
          }

          routeInfo = await this.getRouteInfo(notFoundRoute, notFoundRoute, query, as, {
            shallow: false
          });
        }
      }

      Router.events.emit('beforeHistoryChange', as, routeProps);
      this.changeState(method, url, as, options);

      if (false) {}

      await this.set(route, pathname, query, cleanedAs, routeInfo, forcedScroll || (options.scroll ? {
        x: 0,
        y: 0
      } : null)).catch(e => {
        if (e.cancelled) error = error || e;else throw e;
      });

      if (error) {
        Router.events.emit('routeChangeError', error, cleanedAs, routeProps);
        throw error;
      }

      if (false) {}

      Router.events.emit('routeChangeComplete', as, routeProps);
      return true;
    } catch (err) {
      if (err.cancelled) {
        return false;
      }

      throw err;
    }
  }

  changeState(method, url, as, options = {}) {
    if (false) {}

    if (method !== 'pushState' || (0, _utils.getURL)() !== as) {
      this._shallow = options.shallow;
      window.history[method]({
        url,
        as,
        options,
        __N: true,
        idx: this._idx = method !== 'pushState' ? this._idx : this._idx + 1
      }, // Most browsers currently ignores this parameter, although they may use it in the future.
      // Passing the empty string here should be safe against future changes to the method.
      // https://developer.mozilla.org/en-US/docs/Web/API/History/replaceState
      '', as);
    }
  }

  async handleRouteInfoError(err, pathname, query, as, routeProps, loadErrorFail) {
    if (err.cancelled) {
      // bubble up cancellation errors
      throw err;
    }

    if ((0, _routeLoader.isAssetError)(err) || loadErrorFail) {
      Router.events.emit('routeChangeError', err, as, routeProps); // If we can't load the page it could be one of following reasons
      //  1. Page doesn't exists
      //  2. Page does exist in a different zone
      //  3. Internal error while loading the page
      // So, doing a hard reload is the proper way to deal with this.

      window.location.href = as; // Changing the URL doesn't block executing the current code path.
      // So let's throw a cancellation error stop the routing logic.

      throw buildCancellationError();
    }

    try {
      let Component;
      let styleSheets;
      let props;

      if (typeof Component === 'undefined' || typeof styleSheets === 'undefined') {
        ;
        ({
          page: Component,
          styleSheets
        } = await this.fetchComponent('/_error'));
      }

      const routeInfo = {
        props,
        Component,
        styleSheets,
        err,
        error: err
      };

      if (!routeInfo.props) {
        try {
          routeInfo.props = await this.getInitialProps(Component, {
            err,
            pathname,
            query
          });
        } catch (gipErr) {
          console.error('Error in error page `getInitialProps`: ', gipErr);
          routeInfo.props = {};
        }
      }

      return routeInfo;
    } catch (routeInfoErr) {
      return this.handleRouteInfoError(routeInfoErr, pathname, query, as, routeProps, true);
    }
  }

  async getRouteInfo(route, pathname, query, as, routeProps) {
    try {
      const existingRouteInfo = this.components[route];

      if (routeProps.shallow && existingRouteInfo && this.route === route) {
        return existingRouteInfo;
      }

      const cachedRouteInfo = existingRouteInfo && 'initial' in existingRouteInfo ? undefined : existingRouteInfo;
      const routeInfo = cachedRouteInfo ? cachedRouteInfo : await this.fetchComponent(route).then(res => ({
        Component: res.page,
        styleSheets: res.styleSheets,
        __N_SSG: res.mod.__N_SSG,
        __N_SSP: res.mod.__N_SSP
      }));
      const {
        Component,
        __N_SSG,
        __N_SSP
      } = routeInfo;

      if (false) {}

      let dataHref;

      if (__N_SSG || __N_SSP) {
        dataHref = this.pageLoader.getDataHref((0, _utils.formatWithValidation)({
          pathname,
          query
        }), delBasePath(as), __N_SSG, this.locale);
      }

      const props = await this._getData(() => __N_SSG ? this._getStaticData(dataHref) : __N_SSP ? this._getServerData(dataHref) : this.getInitialProps(Component, // we provide AppTree later so this needs to be `any`
      {
        pathname,
        query,
        asPath: as
      }));
      routeInfo.props = props;
      this.components[route] = routeInfo;
      return routeInfo;
    } catch (err) {
      return this.handleRouteInfoError(err, pathname, query, as, routeProps);
    }
  }

  set(route, pathname, query, as, data, resetScroll) {
    this.isFallback = false;
    this.route = route;
    this.pathname = pathname;
    this.query = query;
    this.asPath = as;
    return this.notify(data, resetScroll);
  }
  /**
  * Callback to execute before replacing router state
  * @param cb callback to be executed
  */


  beforePopState(cb) {
    this._bps = cb;
  }

  onlyAHashChange(as) {
    if (!this.asPath) return false;
    const [oldUrlNoHash, oldHash] = this.asPath.split('#');
    const [newUrlNoHash, newHash] = as.split('#'); // Makes sure we scroll to the provided hash if the url/hash are the same

    if (newHash && oldUrlNoHash === newUrlNoHash && oldHash === newHash) {
      return true;
    } // If the urls are change, there's more than a hash change


    if (oldUrlNoHash !== newUrlNoHash) {
      return false;
    } // If the hash has changed, then it's a hash only change.
    // This check is necessary to handle both the enter and
    // leave hash === '' cases. The identity case falls through
    // and is treated as a next reload.


    return oldHash !== newHash;
  }

  scrollToHash(as) {
    const [, hash] = as.split('#'); // Scroll to top if the hash is just `#` with no value

    if (hash === '') {
      window.scrollTo(0, 0);
      return;
    } // First we check if the element by id is found


    const idEl = document.getElementById(hash);

    if (idEl) {
      idEl.scrollIntoView();
      return;
    } // If there's no element with the id, we check the `name` property
    // To mirror browsers


    const nameEl = document.getElementsByName(hash)[0];

    if (nameEl) {
      nameEl.scrollIntoView();
    }
  }

  urlIsNew(asPath) {
    return this.asPath !== asPath;
  }

  _resolveHref(parsedHref, pages, applyBasePath = true) {
    const {
      pathname
    } = parsedHref;
    const cleanPathname = (0, _normalizeTrailingSlash.removePathTrailingSlash)((0, _denormalizePagePath.denormalizePagePath)(applyBasePath ? delBasePath(pathname) : pathname));

    if (cleanPathname === '/404' || cleanPathname === '/_error') {
      return parsedHref;
    } // handle resolving href for dynamic routes


    if (!pages.includes(cleanPathname)) {
      // eslint-disable-next-line array-callback-return
      pages.some(page => {
        if ((0, _isDynamic.isDynamicRoute)(page) && (0, _routeRegex.getRouteRegex)(page).re.test(cleanPathname)) {
          parsedHref.pathname = applyBasePath ? addBasePath(page) : page;
          return true;
        }
      });
    }

    parsedHref.pathname = (0, _normalizeTrailingSlash.removePathTrailingSlash)(parsedHref.pathname);
    return parsedHref;
  }
  /**
  * Prefetch page code, you may wait for the data during page rendering.
  * This feature only works in production!
  * @param url the href of prefetched page
  * @param asPath the as path of the prefetched page
  */


  async prefetch(url, asPath = url, options = {}) {
    let parsed = (0, _parseRelativeUrl.parseRelativeUrl)(url);
    let {
      pathname
    } = parsed;

    if (false) {}

    const pages = await this.pageLoader.getPageList();
    parsed = this._resolveHref(parsed, pages, false);

    if (parsed.pathname !== pathname) {
      pathname = parsed.pathname;
      url = (0, _utils.formatWithValidation)(parsed);
    } // Prefetch is not supported in development mode because it would trigger on-demand-entries


    if (false) {}

    const route = (0, _normalizeTrailingSlash.removePathTrailingSlash)(pathname);
    await Promise.all([this.pageLoader._isSsg(url).then(isSsg => {
      return isSsg ? this._getStaticData(this.pageLoader.getDataHref(url, asPath, true, typeof options.locale !== 'undefined' ? options.locale : this.locale)) : false;
    }), this.pageLoader[options.priority ? 'loadPage' : 'prefetch'](route)]);
  }

  async fetchComponent(route) {
    let cancelled = false;

    const cancel = this.clc = () => {
      cancelled = true;
    };

    const componentResult = await this.pageLoader.loadPage(route);

    if (cancelled) {
      const error = new Error(`Abort fetching component for route: "${route}"`);
      error.cancelled = true;
      throw error;
    }

    if (cancel === this.clc) {
      this.clc = null;
    }

    return componentResult;
  }

  _getData(fn) {
    let cancelled = false;

    const cancel = () => {
      cancelled = true;
    };

    this.clc = cancel;
    return fn().then(data => {
      if (cancel === this.clc) {
        this.clc = null;
      }

      if (cancelled) {
        const err = new Error('Loading initial props cancelled');
        err.cancelled = true;
        throw err;
      }

      return data;
    });
  }

  _getStaticData(dataHref) {
    const {
      href: cacheKey
    } = new URL(dataHref, window.location.href);

    if ( true && this.sdc[cacheKey]) {
      return Promise.resolve(this.sdc[cacheKey]);
    }

    return fetchNextData(dataHref, this.isSsr).then(data => {
      this.sdc[cacheKey] = data;
      return data;
    });
  }

  _getServerData(dataHref) {
    return fetchNextData(dataHref, this.isSsr);
  }

  getInitialProps(Component, ctx) {
    const {
      Component: App
    } = this.components['/_app'];

    const AppTree = this._wrapApp(App);

    ctx.AppTree = AppTree;
    return (0, _utils.loadGetInitialProps)(App, {
      AppTree,
      Component,
      router: this,
      ctx
    });
  }

  abortComponentLoad(as, routeProps) {
    if (this.clc) {
      Router.events.emit('routeChangeError', buildCancellationError(), as, routeProps);
      this.clc();
      this.clc = null;
    }
  }

  notify(data, resetScroll) {
    return this.sub(data, this.components['/_app'].Component, resetScroll);
  }

}

exports.default = Router;
Router.events = (0, _mitt.default)();

/***/ }),

/***/ 7687:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.formatUrl = formatUrl;

var querystring = _interopRequireWildcard(__webpack_require__(4915));

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function () {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || typeof obj !== "object" && typeof obj !== "function") {
    return {
      default: obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj.default = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
} // Format function modified from nodejs
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.


const slashedProtocols = /https?|ftp|gopher|file/;

function formatUrl(urlObj) {
  let {
    auth,
    hostname
  } = urlObj;
  let protocol = urlObj.protocol || '';
  let pathname = urlObj.pathname || '';
  let hash = urlObj.hash || '';
  let query = urlObj.query || '';
  let host = false;
  auth = auth ? encodeURIComponent(auth).replace(/%3A/i, ':') + '@' : '';

  if (urlObj.host) {
    host = auth + urlObj.host;
  } else if (hostname) {
    host = auth + (~hostname.indexOf(':') ? `[${hostname}]` : hostname);

    if (urlObj.port) {
      host += ':' + urlObj.port;
    }
  }

  if (query && typeof query === 'object') {
    query = String(querystring.urlQueryToSearchParams(query));
  }

  let search = urlObj.search || query && `?${query}` || '';
  if (protocol && protocol.substr(-1) !== ':') protocol += ':';

  if (urlObj.slashes || (!protocol || slashedProtocols.test(protocol)) && host !== false) {
    host = '//' + (host || '');
    if (pathname && pathname[0] !== '/') pathname = '/' + pathname;
  } else if (!host) {
    host = '';
  }

  if (hash && hash[0] !== '#') hash = '#' + hash;
  if (search && search[0] !== '?') search = '?' + search;
  pathname = pathname.replace(/[?#]/g, encodeURIComponent);
  search = search.replace('#', '%23');
  return `${protocol}${host}${pathname}${search}${hash}`;
}

/***/ }),

/***/ 3288:
/***/ (function(__unused_webpack_module, exports) {

"use strict";


exports.__esModule = true;
exports.isDynamicRoute = isDynamicRoute; // Identify /[param]/ in route string

const TEST_ROUTE = /\/\[[^/]+?\](?=\/|$)/;

function isDynamicRoute(route) {
  return TEST_ROUTE.test(route);
}

/***/ }),

/***/ 4436:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.parseRelativeUrl = parseRelativeUrl;

var _utils = __webpack_require__(3937);

var _querystring = __webpack_require__(4915);
/**
* Parses path-relative urls (e.g. `/hello/world?foo=bar`). If url isn't path-relative
* (e.g. `./hello`) then at least base must be.
* Absolute urls are rejected with one exception, in the browser, absolute urls that are on
* the current origin will be parsed as relative
*/


function parseRelativeUrl(url, base) {
  const globalBase = new URL( true ? 'http://n' : 0);
  const resolvedBase = base ? new URL(base, globalBase) : globalBase;
  const {
    pathname,
    searchParams,
    search,
    hash,
    href,
    origin
  } = new URL(url, resolvedBase);

  if (origin !== globalBase.origin) {
    throw new Error(`invariant: invalid relative URL, router received ${url}`);
  }

  return {
    pathname,
    query: (0, _querystring.searchParamsToUrlQuery)(searchParams),
    search,
    hash,
    href: href.slice(globalBase.origin.length)
  };
}

/***/ }),

/***/ 4915:
/***/ (function(__unused_webpack_module, exports) {

"use strict";


exports.__esModule = true;
exports.searchParamsToUrlQuery = searchParamsToUrlQuery;
exports.urlQueryToSearchParams = urlQueryToSearchParams;
exports.assign = assign;

function searchParamsToUrlQuery(searchParams) {
  const query = {};
  searchParams.forEach((value, key) => {
    if (typeof query[key] === 'undefined') {
      query[key] = value;
    } else if (Array.isArray(query[key])) {
      ;
      query[key].push(value);
    } else {
      query[key] = [query[key], value];
    }
  });
  return query;
}

function stringifyUrlQueryParam(param) {
  if (typeof param === 'string' || typeof param === 'number' && !isNaN(param) || typeof param === 'boolean') {
    return String(param);
  } else {
    return '';
  }
}

function urlQueryToSearchParams(urlQuery) {
  const result = new URLSearchParams();
  Object.entries(urlQuery).forEach(([key, value]) => {
    if (Array.isArray(value)) {
      value.forEach(item => result.append(key, stringifyUrlQueryParam(item)));
    } else {
      result.set(key, stringifyUrlQueryParam(value));
    }
  });
  return result;
}

function assign(target, ...searchParamsList) {
  searchParamsList.forEach(searchParams => {
    Array.from(searchParams.keys()).forEach(key => target.delete(key));
    searchParams.forEach((value, key) => target.append(key, value));
  });
  return target;
}

/***/ }),

/***/ 7451:
/***/ (function(__unused_webpack_module, exports) {

"use strict";


exports.__esModule = true;
exports.getRouteMatcher = getRouteMatcher;

function getRouteMatcher(routeRegex) {
  const {
    re,
    groups
  } = routeRegex;
  return pathname => {
    const routeMatch = re.exec(pathname);

    if (!routeMatch) {
      return false;
    }

    const decode = param => {
      try {
        return decodeURIComponent(param);
      } catch (_) {
        const err = new Error('failed to decode param');
        err.code = 'DECODE_FAILED';
        throw err;
      }
    };

    const params = {};
    Object.keys(groups).forEach(slugName => {
      const g = groups[slugName];
      const m = routeMatch[g.pos];

      if (m !== undefined) {
        params[slugName] = ~m.indexOf('/') ? m.split('/').map(entry => decode(entry)) : g.repeat ? [decode(m)] : decode(m);
      }
    });
    return params;
  };
}

/***/ }),

/***/ 8193:
/***/ (function(__unused_webpack_module, exports) {

"use strict";


exports.__esModule = true;
exports.getRouteRegex = getRouteRegex; // this isn't importing the escape-string-regex module
// to reduce bytes

function escapeRegex(str) {
  return str.replace(/[|\\{}()[\]^$+*?.-]/g, '\\$&');
}

function parseParameter(param) {
  const optional = param.startsWith('[') && param.endsWith(']');

  if (optional) {
    param = param.slice(1, -1);
  }

  const repeat = param.startsWith('...');

  if (repeat) {
    param = param.slice(3);
  }

  return {
    key: param,
    repeat,
    optional
  };
}

function getRouteRegex(normalizedRoute) {
  const segments = (normalizedRoute.replace(/\/$/, '') || '/').slice(1).split('/');
  const groups = {};
  let groupIndex = 1;
  const parameterizedRoute = segments.map(segment => {
    if (segment.startsWith('[') && segment.endsWith(']')) {
      const {
        key,
        optional,
        repeat
      } = parseParameter(segment.slice(1, -1));
      groups[key] = {
        pos: groupIndex++,
        repeat,
        optional
      };
      return repeat ? optional ? '(?:/(.+?))?' : '/(.+?)' : '/([^/]+?)';
    } else {
      return `/${escapeRegex(segment)}`;
    }
  }).join(''); // dead code eliminate for browser since it's only needed
  // while generating routes-manifest

  if (true) {
    let routeKeyCharCode = 97;
    let routeKeyCharLength = 1; // builds a minimal routeKey using only a-z and minimal number of characters

    const getSafeRouteKey = () => {
      let routeKey = '';

      for (let i = 0; i < routeKeyCharLength; i++) {
        routeKey += String.fromCharCode(routeKeyCharCode);
        routeKeyCharCode++;

        if (routeKeyCharCode > 122) {
          routeKeyCharLength++;
          routeKeyCharCode = 97;
        }
      }

      return routeKey;
    };

    const routeKeys = {};
    let namedParameterizedRoute = segments.map(segment => {
      if (segment.startsWith('[') && segment.endsWith(']')) {
        const {
          key,
          optional,
          repeat
        } = parseParameter(segment.slice(1, -1)); // replace any non-word characters since they can break
        // the named regex

        let cleanedKey = key.replace(/\W/g, '');
        let invalidKey = false; // check if the key is still invalid and fallback to using a known
        // safe key

        if (cleanedKey.length === 0 || cleanedKey.length > 30) {
          invalidKey = true;
        }

        if (!isNaN(parseInt(cleanedKey.substr(0, 1)))) {
          invalidKey = true;
        }

        if (invalidKey) {
          cleanedKey = getSafeRouteKey();
        }

        routeKeys[cleanedKey] = key;
        return repeat ? optional ? `(?:/(?<${cleanedKey}>.+?))?` : `/(?<${cleanedKey}>.+?)` : `/(?<${cleanedKey}>[^/]+?)`;
      } else {
        return `/${escapeRegex(segment)}`;
      }
    }).join('');
    return {
      re: new RegExp(`^${parameterizedRoute}(?:/)?$`),
      groups,
      routeKeys,
      namedRegex: `^${namedParameterizedRoute}(?:/)?$`
    };
  }

  return {
    re: new RegExp(`^${parameterizedRoute}(?:/)?$`),
    groups
  };
}

/***/ }),

/***/ 3937:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.execOnce = execOnce;
exports.getLocationOrigin = getLocationOrigin;
exports.getURL = getURL;
exports.getDisplayName = getDisplayName;
exports.isResSent = isResSent;
exports.loadGetInitialProps = loadGetInitialProps;
exports.formatWithValidation = formatWithValidation;
exports.ST = exports.SP = exports.urlObjectKeys = void 0;

var _formatUrl = __webpack_require__(7687);
/**
* Utils
*/


function execOnce(fn) {
  let used = false;
  let result;
  return (...args) => {
    if (!used) {
      used = true;
      result = fn(...args);
    }

    return result;
  };
}

function getLocationOrigin() {
  const {
    protocol,
    hostname,
    port
  } = window.location;
  return `${protocol}//${hostname}${port ? ':' + port : ''}`;
}

function getURL() {
  const {
    href
  } = window.location;
  const origin = getLocationOrigin();
  return href.substring(origin.length);
}

function getDisplayName(Component) {
  return typeof Component === 'string' ? Component : Component.displayName || Component.name || 'Unknown';
}

function isResSent(res) {
  return res.finished || res.headersSent;
}

async function loadGetInitialProps(App, ctx) {
  if (false) { var _App$prototype; } // when called from _app `ctx` is nested in `ctx`


  const res = ctx.res || ctx.ctx && ctx.ctx.res;

  if (!App.getInitialProps) {
    if (ctx.ctx && ctx.Component) {
      // @ts-ignore pageProps default
      return {
        pageProps: await loadGetInitialProps(ctx.Component, ctx.ctx)
      };
    }

    return {};
  }

  const props = await App.getInitialProps(ctx);

  if (res && isResSent(res)) {
    return props;
  }

  if (!props) {
    const message = `"${getDisplayName(App)}.getInitialProps()" should resolve to an object. But found "${props}" instead.`;
    throw new Error(message);
  }

  if (false) {}

  return props;
}

const urlObjectKeys = ['auth', 'hash', 'host', 'hostname', 'href', 'path', 'pathname', 'port', 'protocol', 'query', 'search', 'slashes'];
exports.urlObjectKeys = urlObjectKeys;

function formatWithValidation(url) {
  if (false) {}

  return (0, _formatUrl.formatUrl)(url);
}

const SP = typeof performance !== 'undefined';
exports.SP = SP;
const ST = SP && typeof performance.mark === 'function' && typeof performance.measure === 'function';
exports.ST = ST;

/***/ }),

/***/ 7770:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ pages; }
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(79);
// EXTERNAL MODULE: external "redux-saga"
var external_redux_saga_ = __webpack_require__(7765);
// EXTERNAL MODULE: ./store.js + 14 modules
var store = __webpack_require__(1645);
;// CONCATENATED MODULE: external "next/head"
var head_namespaceObject = require("next/head");;
var head_default = /*#__PURE__*/__webpack_require__.n(head_namespaceObject);
// EXTERNAL MODULE: external "@ant-design/icons"
var icons_ = __webpack_require__(2372);
// EXTERNAL MODULE: ./components/header/Header.module.scss
var Header_module = __webpack_require__(4118);
var Header_module_default = /*#__PURE__*/__webpack_require__.n(Header_module);
// EXTERNAL MODULE: external "antd"
var external_antd_ = __webpack_require__(953);
// EXTERNAL MODULE: ./const/actions.ts
var actions = __webpack_require__(7030);
;// CONCATENATED MODULE: ./components/header/Header.tsx
var __jsx = (external_react_default()).createElement;







//tmu
const branchEducationTMU = [{
  name: "Quản trị kinh doanh",
  id: 2,
  fts: '"Kinh doanh"'
}, {
  name: "Quản trị khách sạn",
  id: 5,
  fts: '"Lễ tân" "hành chính"'
}, {
  name: "Quản trị dịch vụ du lịch và lữ hành",
  id: 5
}, {
  name: "Marketing",
  id: 9,
  fts: "marketing"
}, {
  name: "Logistics và Quản lý chuỗi cung ứng",
  id: -1,
  majorID: [33],
  fts: 'logistics'
}, {
  name: "Kế toán - Kiểm toán",
  id: 8,
  fts: '"Kế toán"'
}, {
  name: "Kinh doanh quốc tế",
  id: 2,
  fts: '"Kinh doanh"'
} // { name: "Kinh tế quốc tế", id: 31 }, 
];
const branchEducationTMU2 = [{
  name: "Quản lý kinh tế",
  id: 31,
  fts: '"thị trường" "đầu tư" "chứng khoán" "phân tích" "nghiệp vụ" "tài chính" "tư vấn tài chính"'
}, {
  name: "Tài chính - Ngân hàng",
  id: 3,
  fts: '"tín dụng" "giao dịch" "rủi ro"	"ngân hàng" "tài chính" "tư vấn tài chính" "cá nhân"'
}, {
  name: "Thương mại điện tử",
  id: 2,
  fts: '"commerce" "ecommerce" "thương mại điện tử" "Shopee"'
}, {
  name: "Tiếng Anh thương mại",
  id: 22,
  searchByHighLight: true
}, {
  name: "Luật kinh tế",
  id: -1,
  majorID: 7,
  fts: '"pháp lý" "pháp chế" "luật" "hồ sơ"'
}, {
  name: "Hệ thống thông tin quản lý",
  id: 1,
  majorID: [329]
}, {
  name: "Quản trị nhân lực",
  id: 6,
  fts: '"Nhân sự" "HR" "Tuyển dụng" "hành chính"'
}];

const Header = ({
  logo,
  primaryColor,
  primaryDarkColor,
  param,
  schoolID,
  getEventHotJob
}) => {
  const dispatch = (0,external_react_redux_.useDispatch)(); // useEffect(() => {
  //     console.log('vao useEffect')
  //     dispatch({type: REDUX_SAGA.EVENT.JOB.HOT})
  // }, []);

  const clickBranchTMU = e => {// dispatch({type: REDUX_SAGA.EVENT.JOB.HOT})
    // getEventHotJob(0,21)
  };

  return __jsx((external_react_default()).Fragment, null, __jsx("div", {
    className: (Header_module_default()).header,
    style: {
      backgroundColor: primaryColor
    }
  }, __jsx("div", {
    className: (Header_module_default()).directPage,
    style: {
      left: '7%'
    }
  }, __jsx("div", {
    style: {
      display: "block"
    }
  }, __jsx("a", {
    href: "/",
    style: {
      backgroundColor: 'rgb(0, 125, 187)'
    }
  }, __jsx(icons_.TagOutlined, null), " Trang ch\u1EE7"), __jsx("a", {
    href: "/"
  }, __jsx(icons_.HomeOutlined, null), " Vi\u1EC7c l\xE0m Works.vn"), __jsx("a", {
    href: "https://play.google.com/store/apps/details?id=com.worksvn.student&hl=en_US",
    target: "_blank",
    rel: "noopener noreferrer"
  }, __jsx(icons_.AndroidFilled, {
    style: {
      fontSize: '16.3px'
    }
  }), " \u1EE8ng d\u1EE5ng Android"), __jsx("a", {
    href: "https://apps.apple.com/vn/app/worksvn-sinh-vi%C3%AAn/id1492437454",
    target: "_blank",
    rel: "noopener noreferrer"
  }, __jsx(icons_.AppleFilled, {
    style: {
      fontSize: '16.8px'
    }
  }), " \u1EE8ng d\u1EE5ng iOS"), __jsx(external_antd_.Dropdown, {
    overlay: __jsx("div", {
      style: {
        display: 'flex'
      }
    }, __jsx(external_antd_.Menu, null, branchEducationTMU.map((e, i) => {
      return __jsx(external_antd_.Menu.Item, {
        key: i
      }, __jsx("a", {
        rel: "noopener noreferrer",
        href: "#id_schedule_a",
        onClick: clickBranchTMU.bind(null, e)
      }, e.name));
    })), __jsx(external_antd_.Menu, null, branchEducationTMU2.map((e, i) => {
      return __jsx(external_antd_.Menu.Item, {
        key: i
      }, __jsx("a", {
        rel: "noopener noreferrer",
        href: "#id_schedule_a",
        onClick: clickBranchTMU.bind(null, e)
      }, e.name));
    }))),
    trigger: ['click']
  }, __jsx("a", {
    className: "wapper-education"
  }, __jsx(icons_.BookOutlined, null), " Vi\u1EC7c l\xE0m theo nh\xF3m ng\xE0nh")))), __jsx("div", {
    className: (Header_module_default()).function,
    style: {
      display: "flex"
    }
  }, __jsx("span", {
    className: `${(Header_module_default()).labelLogin} hidden-mobile`
  }, __jsx("a", null, "\u0110\u0103ng nh\u1EADp / \u0110\u0103ng k\xFD")))));
}; // export default Header


const mapStateToProps = state => ({
  logo: state.DetailEvent.logo,
  primaryColor: state.DetailEvent.primaryColor,
  primaryDarkColor: state.DetailEvent.primaryDarkColor,
  param: state.DetailEvent.param,
  schoolID: state.DetailEvent.schoolID
});

const mapDispatchToProps = dispatch => ({
  getEventHotJob: (pageIndex, pageSize) => dispatch({
    type: actions/* REDUX_SAGA.EVENT.JOB.HOT */.A.EVENT.JOB.HOT,
    pageIndex,
    pageSize
  })
});

/* harmony default export */ var header_Header = ((0,external_react_redux_.connect)(mapStateToProps, mapDispatchToProps)(Header));
// EXTERNAL MODULE: ./components/banner/Banner.module.scss
var Banner_module = __webpack_require__(459);
var Banner_module_default = /*#__PURE__*/__webpack_require__.n(Banner_module);
// EXTERNAL MODULE: ./assets/image/CHOINGAY.png
var CHOINGAY = __webpack_require__(8160);
var CHOINGAY_default = /*#__PURE__*/__webpack_require__.n(CHOINGAY);
;// CONCATENATED MODULE: ./components/banner/Banner.tsx

var Banner_jsx = (external_react_default()).createElement;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }




function Banner() {
  const props = {
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 400
  };
  return Banner_jsx((external_react_default()).Fragment, null, Banner_jsx("div", {
    className: "employer-banner",
    style: {
      width: '100%',
      position: 'relative'
    }
  }, Banner_jsx(external_antd_.Carousel, _extends({
    dots: true
  }, props, {
    autoplay: true
  }), Banner_jsx("div", {
    className: (Banner_module_default()).banner
  }, Banner_jsx("div", {
    className: (Banner_module_default()).wapperButtonPlayGame
  }, Banner_jsx("img", {
    className: (Banner_module_default()).buttonPlayGame,
    src: (CHOINGAY_default())
  })), Banner_jsx("img", {
    src: 'https://storage.googleapis.com/worksvn-prod.appspot.com/temp/LEKYKET_tlu.png',
    key: 2,
    alt: "banner",
    className: (Banner_module_default()).imageBanner
  })), Banner_jsx("div", {
    className: (Banner_module_default()).banner
  }, Banner_jsx("img", {
    src: 'https://storage.googleapis.com/worksvn-prod.appspot.com/school_events/employer_banners/74d57629-3b0a-472f-9ab3-284987ce72e6/banner_1610351469285',
    key: 1,
    alt: "banner",
    className: (Banner_module_default()).imageBanner
  })))));
}
// EXTERNAL MODULE: ./components/top-job-em/TopJobEm.module.scss
var TopJobEm_module = __webpack_require__(335);
var TopJobEm_module_default = /*#__PURE__*/__webpack_require__.n(TopJobEm_module);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: ./utils/convertNumber.js
var convertNumber = __webpack_require__(5784);
// EXTERNAL MODULE: ./assets/image/base-image.jpg
var base_image = __webpack_require__(9388);
var base_image_default = /*#__PURE__*/__webpack_require__.n(base_image);
// EXTERNAL MODULE: ./components/layout/common/Common.tsx
var Common = __webpack_require__(819);
;// CONCATENATED MODULE: ./components/top-job-em/TopJob.tsx
var TopJob_jsx = (external_react_default()).createElement;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




 // import { Link } from "react-router-dom";


 //@ts-ignore






class TopJob extends external_react_.PureComponent {
  constructor(props) {
    super(props);

    _defineProperty(this, "changePage", event => {
      this.props.getEventHotJob(event - 1, 10); // this.setState({
      //     current: event,
      // });

      this.props.setCurrentPage(event); // window.location.assign('#id_schedule_b')
    });

    this.state = {
      current: 1
    };
  }

  componentDidMount() {
    console.log('vao componentDidMount'); // this.props.getEventHotJob(0, 10);
  }

  render() {
    let {
      topJob,
      param,
      currentPage
    } = this.props;
    console.log(currentPage);
    return TopJob_jsx(external_antd_.Row, {
      className: (TopJobEm_module_default()).homeJob,
      style: {
        display: topJob.totalItems === 0 ? "none" : ""
      },
      id: "homeJob"
    }, topJob && topJob.items ? topJob.items.map((item, index) => {
      let logoUrl = item.employerLogoUrl;

      if (!logoUrl) {
        logoUrl = (base_image_default());
      }

      return TopJob_jsx(external_antd_.Col, {
        xs: 24,
        sm: 24,
        md: 12,
        lg: 12,
        xl: 12,
        xxl: 12,
        key: index
      }, TopJob_jsx("div", {
        key: index,
        className: (TopJobEm_module_default()).hJItemTop,
        onClick: () => {// window.open(`/chi-tiet-cong-viec/${window.btoa(item.id)}${param}`)
        }
      }, TopJob_jsx("div", {
        className: (TopJobEm_module_default()).imgJob
      }, TopJob_jsx("img", {
        src: logoUrl,
        alt: "employer logo"
      }), TopJob_jsx(Common/* JobType */.O4, null, item.jobType)), TopJob_jsx("div", null, TopJob_jsx("div", {
        className: (TopJobEm_module_default()).jobContent
      }, TopJob_jsx("ul", null, TopJob_jsx("li", {
        className: (TopJobEm_module_default()).jD
      }, TopJob_jsx(next_link.default // to={`/chi-tiet-cong-viec/${window.btoa(item.id)}${param}`}
      // target="_blank"
      , {
        href: "/chi-tiet-cong-viec/1"
      }, TopJob_jsx("h6", {
        className: (TopJobEm_module_default()).lC,
        style: {
          color: item.titleHighlight ? "red" : "black"
        }
      }, TopJob_jsx("span", {
        className: (TopJobEm_module_default()).topBadge,
        style: {
          marginRight: 5
        }
      }, "Hot"), item.jobTitle))), TopJob_jsx("li", {
        className: (TopJobEm_module_default()).lC
      }, TopJob_jsx(next_link.default, {
        href: "/about"
      }, TopJob_jsx("a", {
        onClick: () => {
          if (window.innerWidth > 768) {// window.open(`/employer/${window.btoa(item.employerID)}${param}`)
          }
        },
        className: (TopJobEm_module_default()).aNameEmployer,
        style: {
          textTransform: 'capitalize',
          fontSize: '0.8em'
        }
      }, item.employerName))), TopJob_jsx("li", {
        className: (TopJobEm_module_default()).region
      }, TopJob_jsx(icons_.EnvironmentOutlined, {
        style: {
          marginRight: 3
        }
      }), item.region && item.region.name ? item.region.name : null), TopJob_jsx("li", {
        className: (TopJobEm_module_default()).salary
      }, TopJob_jsx(icons_.DollarOutlined, {
        style: {
          marginRight: 3
        }
      }), TopJob_jsx("span", {
        className: (TopJobEm_module_default()).salaryLabel
      }, TopJob_jsx("b", null, (0,convertNumber/* convertFullSalary */.kE)(item.minSalary, item.minSalaryUnit, item.maxSalary, item.maxSalaryUnit)))))))), " ");
    }) : null, topJob.totalItems / topJob.pageSize > 1 ? TopJob_jsx(external_antd_.Col, {
      span: 24,
      style: {
        textAlign: "center"
      }
    }, TopJob_jsx(external_antd_.Pagination, {
      current: currentPage,
      pageSize: topJob.pageSize,
      total: topJob.totalItems,
      style: {
        margin: "25px 0px 10px"
      },
      onChange: this.changePage
    })) : null);
  }

}

const TopJob_mapStateToProps = state => ({
  topJob: state.EventHotJobResults.data,
  currentPage: state.EventHotJobResults.currentPage // param: state.DetailEvent.param,

});

const TopJob_mapDispatchToProps = dispatch => ({
  getEventHotJob: (pageIndex, pageSize) => dispatch({
    type: actions/* REDUX_SAGA.EVENT.JOB.HOT */.A.EVENT.JOB.HOT,
    pageIndex,
    pageSize
  }),
  setCurrentPage: currentPage => dispatch({
    type: actions/* REDUX.EVENT.JOB.HOT_CURRENT_PAGE */.r.EVENT.JOB.HOT_CURRENT_PAGE,
    currentPage
  })
});

/* harmony default export */ var top_job_em_TopJob = ((0,external_react_redux_.connect)(TopJob_mapStateToProps, TopJob_mapDispatchToProps)(TopJob));
;// CONCATENATED MODULE: ./components/test/test.tsx
var test_jsx = (external_react_default()).createElement;



// import jQuery from '../top-job-em/marquee'
const Test = ({
  topJob
}) => {
  const dispatch = (0,external_react_redux_.useDispatch)();
  (0,external_react_.useEffect)(() => {
    // console.log('vao useEffect Test 2')
    // dispatch({ type: REDUX_SAGA.EVENT.JOB.HOT })
    console.log(window.innerHeight);
    window.$(document).ready(function () {
      console.log('document ready');
      window.$(".marquee").marquee({
        direction: 'up',
        duplicated: true,
        speed: 40,
        pauseOnHover: true
      });
    });
  }, []); // const router = useRouter()

  return test_jsx((external_react_default()).Fragment, null, test_jsx("div", {
    class: "marquee"
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit END."));
};

const test_mapStateToProps = state => ({
  topJob: state.EventHotJobResults.data
});

/* harmony default export */ var test = ((0,external_react_redux_.connect)(test_mapStateToProps)(Test));
;// CONCATENATED MODULE: ./pages/index.js

var pages_jsx = (external_react_default()).createElement;











const Index = () => {
  const dispatch = (0,external_react_redux_.useDispatch)();
  (0,external_react_.useEffect)(() => {
    console.log('vao useEffect Index');
  }, []); // const {data} = useSelector(state => state.EventHotJobResults);
  // console.log(data)

  return pages_jsx((external_react_default()).Fragment, null, pages_jsx((head_default()), null, pages_jsx("script", {
    src: "https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"
  }), pages_jsx("script", {
    type: "text/javascript",
    src: "https://cdn.jsdelivr.net/npm/jquery.marquee@1.5.0/jquery.marquee.min.js"
  })), pages_jsx(header_Header, null), pages_jsx(Banner, null), pages_jsx(top_job_em_TopJob, null), pages_jsx(test, null));
}; // export const getServerSideProps = wrapper.getServerSideProps(async ({store}) => {
//   store.dispatch({type: REDUX_SAGA.EVENT.JOB.HOT});
//   store.dispatch(END);
//   await store.sagaTask.toPromise();
// });


/* harmony default export */ var pages = (Index);

/***/ }),

/***/ 9032:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "$": function() { return /* binding */ authHeaders; },
  "W": function() { return /* binding */ noInfoHeader; }
});

;// CONCATENATED MODULE: ./const/check-server.ts
const ISSERVER = true;
;// CONCATENATED MODULE: ./services/auth.ts
 // No info header

const noInfoHeader = {
  'Access-Control-Allow-Headers': '*',
  "Content-Type": "application/json"
}; // Check invalid header

const authHeaders = {
  'Access-Control-Allow-Headers': '*',
  'Authorization': `Bearer ${!ISSERVER ? localStorage.getItem("accessToken") : null}`
};

/***/ }),

/***/ 7001:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "ZJ": function() { return /* binding */ _delete; },
  "mR": function() { return /* binding */ _get; },
  "d8": function() { return /* binding */ _post; },
  "SQ": function() { return /* binding */ _put; }
});

;// CONCATENATED MODULE: external "axios"
var external_axios_namespaceObject = require("axios");;
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_namespaceObject);
// EXTERNAL MODULE: ./services/auth.ts + 1 modules
var auth = __webpack_require__(9032);
;// CONCATENATED MODULE: ./services/base-api.ts

 // POST

const _post = async (data, api, another_host, headers, params) => {
  let requestURL = (another_host ? another_host : process.env.REACT_APP_API_HOST) + api;

  if (headers === null || headers === undefined) {
    headers = auth/* authHeaders */.$;
  }

  let response = await external_axios_default().post(requestURL, data, {
    headers,
    params
  });
  return response.data;
}; //GET

const _get = async (params, api, another_host, headers) => {
  let requestURL = (another_host ? another_host : process.env.REACT_APP_API_HOST) + api;

  if (headers === null || headers === undefined) {
    headers = auth/* authHeaders */.$;
  }

  let response = await external_axios_default().get(requestURL, {
    params: params,
    headers
  });
  return response.data;
}; // DELETE

const _delete = async (data, api, another_host, headers, params) => {
  let requestURL = (another_host ? another_host : process.env.REACT_APP_API_HOST) + api;

  if (headers === null || headers === undefined) {
    headers = auth/* authHeaders */.$;
  }

  let response = await external_axios_default().delete(requestURL, {
    data: params,
    headers
  });
  return response.data;
}; // PUT

const _put = async (data, api, another_host, headers, params) => {
  let requestURL = (another_host ? another_host : process.env.REACT_APP_API_HOST) + api;

  if (headers === null || headers === undefined) {
    headers = auth/* authHeaders */.$;
  }

  let response = await external_axios_default().put(requestURL, data, {
    headers,
    params
  });
  return response.data;
};

/***/ }),

/***/ 1645:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "YS": function() { return /* binding */ wrapper; }
});

// UNUSED EXPORTS: makeStore, store

;// CONCATENATED MODULE: external "redux"
var external_redux_namespaceObject = require("redux");;
// EXTERNAL MODULE: external "redux-saga"
var external_redux_saga_ = __webpack_require__(7765);
var external_redux_saga_default = /*#__PURE__*/__webpack_require__.n(external_redux_saga_);
;// CONCATENATED MODULE: external "next-redux-wrapper"
var external_next_redux_wrapper_namespaceObject = require("next-redux-wrapper");;
// EXTERNAL MODULE: ./const/actions.ts
var actions = __webpack_require__(7030);
;// CONCATENATED MODULE: ./redux/reducers/event/job.ts
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


let initListEventHotJob = {
  data: {
    pageIndex: 0,
    pageSize: 0,
    totalItems: 0,
    items: []
  },
  loading: true,
  currentPage: 1
};
const EventHotJobResults = (state = initListEventHotJob, action) => {
  switch (action.type) {
    case actions/* REDUX.EVENT.JOB.HOT */.r.EVENT.JOB.HOT:
      return _objectSpread(_objectSpread({}, state), {}, {
        data: action.data
      });

    case actions/* REDUX.EVENT.JOB.HOT_CURRENT_PAGE */.r.EVENT.JOB.HOT_CURRENT_PAGE:
      return _objectSpread(_objectSpread({}, state), {}, {
        currentPage: action.currentPage
      });

    case actions/* REDUX.EVENT.JOB.HOT_LOADING */.r.EVENT.JOB.HOT_LOADING:
      return _objectSpread(_objectSpread({}, state), {}, {
        loading: action.loading
      });

    default:
      return _objectSpread({}, state);
  }
};
;// CONCATENATED MODULE: ./redux/reducers/event/detail.ts
function detail_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function detail_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { detail_ownKeys(Object(source), true).forEach(function (key) { detail_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { detail_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function detail_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


let initListDetailEvent = {
  logo: null,
  primaryColor: '#1890ff',
  primaryDarkColor: '#296ea7',
  param: '',
  schoolID: '',
  eventID: '',
  name: ''
};
const DetailEvent = (state = initListDetailEvent, action) => {
  switch (action.type) {
    case actions/* REDUX.EVENT.LOGO_SCHOOL */.r.EVENT.LOGO_SCHOOL:
      return detail_objectSpread(detail_objectSpread({}, state), {}, {
        logo: action.logo,
        primaryColor: action.primaryColor,
        primaryDarkColor: action.primaryDarkColor,
        param: action.param,
        schoolID: action.schoolID,
        eventID: action.eventID,
        name: action.name
      });

    default:
      return detail_objectSpread({}, state);
  }
};
;// CONCATENATED MODULE: ./redux/reducers/auth.ts
function auth_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function auth_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { auth_ownKeys(Object(source), true).forEach(function (key) { auth_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { auth_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function auth_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


let initAuth = {
  isAuthen: false
};
const AuthState = (state = initAuth, action) => {
  switch (action.type) {
    case actions/* REDUX.AUTHEN.EXACT_AUTHEN */.r.AUTHEN.EXACT_AUTHEN:
      return auth_objectSpread(auth_objectSpread({}, state), {}, {
        isAuthen: true
      });

    case actions/* REDUX.AUTHEN.FAIL_AUTHEN */.r.AUTHEN.FAIL_AUTHEN:
      return auth_objectSpread(auth_objectSpread({}, state), {}, {
        isAuthen: false
      });

    default:
      return state;
  }
};
;// CONCATENATED MODULE: ./redux/store/reducer.ts
function reducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function reducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { reducer_ownKeys(Object(source), true).forEach(function (key) { reducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { reducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function reducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






let rootReducer = {
  EventHotJobResults: EventHotJobResults,
  DetailEvent: DetailEvent,
  AuthState: AuthState
};
const myReducer = (0,external_redux_namespaceObject.combineReducers)(rootReducer);

const newReducer = (state, action) => {
  if (action.type === external_next_redux_wrapper_namespaceObject.HYDRATE) {
    const nextState = reducer_objectSpread(reducer_objectSpread({}, state), action.payload); // if (state.faqCategories && state.faqCategories.categories.length > 0)
    //   nextState.faqCategories = state.faqCategories; // preserve categories on client side navigation


    return nextState;
  } else {
    return myReducer(state, action);
  }
};

/* harmony default export */ var reducer = (newReducer);
;// CONCATENATED MODULE: external "redux-saga/effects"
var effects_namespaceObject = require("redux-saga/effects");;
;// CONCATENATED MODULE: ./const/method.ts
const GET = 'GET';
const POST = 'POST';
const PUT = 'PUT';
const DELETE = 'DELETE';
// EXTERNAL MODULE: ./services/base-api.ts + 1 modules
var base_api = __webpack_require__(7001);
;// CONCATENATED MODULE: external "sweetalert"
var external_sweetalert_namespaceObject = require("sweetalert");;
var external_sweetalert_default = /*#__PURE__*/__webpack_require__.n(external_sweetalert_namespaceObject);
;// CONCATENATED MODULE: ./const/type.ts
const TYPE = {
  SUCCESS: "success",
  ERROR: "error",
  WARNING: "warning",
  FULLTIME: "FULLTIME",
  PARTTIME: "PARTTIME",
  INTERNSHIP: "INTERNSHIP",
  MON: 'MON',
  TUE: 'TUE',
  WED: 'WED',
  THU: 'THU',
  FRI: 'FRI',
  SAT: 'SAT',
  SUN: 'SUN',
  MOR: 'MOR',
  AFT: 'AFT',
  EVN: 'EVN',
  TOP: 'TOP',
  HIGHLIGHT: 'HIGHLIGHT',
  EVENT: 'EVENT',
  NORMAL: 'NORMAL'
};
;// CONCATENATED MODULE: ./services/exception.ts


const exceptionShowNoti = async (err, hide_alert_error) => {
  // console.log(err);
  if (err && err && err.response && err.response.data && !hide_alert_error) {
    let msg = err.response.data.msg;
    external_sweetalert_default()({
      title: "Worksvn thông báo",
      text: msg,
      icon: TYPE.ERROR,
      dangerMode: true
    });
  }
};
;// CONCATENATED MODULE: ./services/exec.ts





const _requestToServer = async (method, data, api, host, headers, params, show_alert, log_query, hide_alert_error) => {
  let res;

  try {
    switch (method) {
      case GET:
        res = await (0,base_api/* _get */.mR)(params, api, host, headers);
        break;

      case POST:
        res = await (0,base_api/* _post */.d8)(data, api, host, headers, params);

        if (show_alert) {
          external_sweetalert_default()({
            title: "Worksvn thông báo",
            text: `${res.msg}`,
            icon: TYPE.SUCCESS,
            dangerMode: false
          });
        }

        break;

      case PUT:
        res = await (0,base_api/* _put */.SQ)(data, api, host, headers, params);

        if (show_alert) {
          external_sweetalert_default()({
            title: "Worksvn thông báo",
            text: `Cập nhập ${res.msg}`,
            icon: TYPE.SUCCESS,
            dangerMode: false
          });
        }

        return res;

      case DELETE:
        res = await (0,base_api/* _delete */.ZJ)(data, api, host, headers, params);
        break;

      default:
        break;
    }

    if (show_alert && res) {
      external_sweetalert_default()({
        title: "worksvn thông báo",
        text: res.msg,
        icon: TYPE.SUCCESS,
        dangerMode: false
      });
    }

    if (log_query) {// console.log(host + api);
      // console.log(params);
      // console.log(data);
      // console.log(res);
    }
  } catch (err) {
    console.log(err);
    console.log(err.response && err.response.data);
    exceptionShowNoti(err, hide_alert_error);
  }

  return res;
};
// EXTERNAL MODULE: ./services/auth.ts + 1 modules
var auth = __webpack_require__(9032);
// EXTERNAL MODULE: ./environment/development.js
var development = __webpack_require__(4151);
;// CONCATENATED MODULE: ./redux/watcher/event/job.ts







function* getListHotJobData(action) {
  // console.log('vao getListHotJobData')
  yield (0,effects_namespaceObject.put)({
    type: actions/* REDUX.EVENT.JOB.HOT_LOADING */.r.EVENT.JOB.HOT_LOADING,
    loading: true
  });
  let res = yield (0,effects_namespaceObject.call)(getHotJobData, action);

  if (res) {
    let data = res.data;
    yield (0,effects_namespaceObject.put)({
      type: actions/* REDUX.EVENT.JOB.HOT */.r.EVENT.JOB.HOT,
      data
    });
  }

  yield (0,effects_namespaceObject.put)({
    type: actions/* REDUX.EVENT.JOB.HOT_LOADING */.r.EVENT.JOB.HOT_LOADING,
    loading: false
  });
}

function getHotJobData(action) {
  let res = _requestToServer(POST, {}, `/api/jobs/active`, development/* PUBLIC_HOST */.U4, auth/* noInfoHeader */.W, {
    pageIndex: action.pageIndex ? action.pageIndex : 0,
    pageSize: action.pageSize ? action.pageSize : 21,
    priority: 'TOP'
  }, false);

  return res;
}

function* EventHotJobWatcher() {
  yield (0,effects_namespaceObject.takeEvery)(actions/* REDUX_SAGA.EVENT.JOB.HOT */.A.EVENT.JOB.HOT, getListHotJobData);
}
;// CONCATENATED MODULE: ./redux/store/saga.ts


function* rootSaga() {
  try {
    yield (0,effects_namespaceObject.all)([EventHotJobWatcher()]);
  } catch (err) {
    console.log(err);
    throw err;
  }
}
;// CONCATENATED MODULE: ./store.js






const bindMiddleware = middleware => {
  // if (process.env.NODE_ENV !== 'production') {
  //   const { composeWithDevTools } = require('redux-devtools-extension')
  //   return composeWithDevTools(applyMiddleware(...middleware))
  // }
  return (0,external_redux_namespaceObject.applyMiddleware)(...middleware);
};

const sagaMiddleware = external_redux_saga_default()();
const store = (0,external_redux_namespaceObject.createStore)(reducer, bindMiddleware([sagaMiddleware]));
const makeStore = context => {
  store.sagaTask = sagaMiddleware.run(rootSaga);
  return store;
};
const wrapper = (0,external_next_redux_wrapper_namespaceObject.createWrapper)(makeStore, {
  debug: true
});

/***/ }),

/***/ 5784:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "kE": function() { return /* binding */ convertFullSalary; },
/* harmony export */   "iF": function() { return /* binding */ convertSalary; }
/* harmony export */ });
/* unused harmony export getNumberWithDot */
const getNumberWithDot = x => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
const convertFullSalary = (min, minUnit, max, maxUnit) => {
  let result = '';

  if (min) {
    result += getNumberWithDot(min);

    if (minUnit !== maxUnit) {
      result += '/' + minUnit;
    }
  }

  if (max) {
    if (result) {
      result += ' - ';
    }

    result += getNumberWithDot(max);

    if (maxUnit) {
      result += '/' + maxUnit;
    }
  }

  if (!result) {
    result = 'Thỏa thuận';
  }

  return result;
};
const convertSalary = (min, max, unit) => {
  return convertFullSalary(min, null, max, unit);
};

/***/ }),

/***/ 9320:
/***/ (function(__unused_webpack_module, exports) {

"use strict";
exports.__esModule=true;exports.normalizePathSep=normalizePathSep;exports.denormalizePagePath=denormalizePagePath;function normalizePathSep(path){return path.replace(/\\/g,'/');}function denormalizePagePath(page){page=normalizePathSep(page);if(page.startsWith('/index/')){page=page.slice(6);}else if(page==='/index'){page='/';}return page;}
//# sourceMappingURL=denormalize-page-path.js.map

/***/ }),

/***/ 1664:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

module.exports = __webpack_require__(6071)


/***/ }),

/***/ 8160:
/***/ (function(module) {

module.exports = "/_next/static/images/CHOINGAY-c83dca83c54b1bffb5da9e5b43623555.png";

/***/ }),

/***/ 9388:
/***/ (function(module) {

module.exports = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD//gAfQ29tcHJlc3NlZCBieSBqcGVnLXJlY29tcHJlc3P/2wCEAAQEBAQEBAQEBAQGBgUGBggHBwcHCAwJCQkJCQwTDA4MDA4MExEUEA8QFBEeFxUVFx4iHRsdIiolJSo0MjRERFwBBAQEBAQEBAQEBAYGBQYGCAcHBwcIDAkJCQkJDBMMDgwMDgwTERQQDxAUER4XFRUXHiIdGx0iKiUlKjQyNEREXP/CABEIAYACAAMBIgACEQEDEQH/xAAcAAEBAQADAQEBAAAAAAAAAAAABwUCAwYEAQj/2gAIAQEAAAAA/v4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdU3zgAAAaNI7QAAAAASzM5gAAA4adTAAAAABGPvq/wCgAAD8k/w2b9AAAAACMaVVOPDnyAABKc6zgAAAAAjGlVWHO+nsou6AAEpzrOAAAAACMaVV/I7wOVj/AEAASnOs4AAAAAIxpVXNlYUzaA+TzXsAJTnWcAAAAAEY0qr1R4K59QPklfR7L2gJTnWcAAAAAEY0qq8X409d7gHySrpPZe0CU51nAAAAABGNKqnw5OpoA+SV9Aey9oJTnWcAAAAAEY0qqAHySrpA9l7QlOdZwAAAAARjSqocfM+n/XySvoAPZe0SnOs4AAAAAIxpVUcZjk+u9v8ALKukAKroynOs4AAAAAIxpVU4zTGPS4PQACoa0pzrOAAAAACMaVVfkwyQAAVDWlOdZwAAAAARjSqv5MMkAACoa0pzrOAAAAACMaVVypcAAAqGtKc6zgAAAAAjGlVcyWAAAKhrSnOs4AAAAAIxpVX4Z+AAA99oSnOs4AAAAAIxpVUAAAEpzrOAAAAAHGOdtD5AAADjPOqx8gAAAAA8P5EAAAB6z3QAAAAAPzDz+LlxcuLlxcuLlxcuLlx5aO3+gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAhAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/9oACAEDEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/EAEYQAAECAwEJDQYEBQUBAQAAAAECAwAEBREGFjE1QVNUkbEQEhMUITNQYXJ0kpPSICNCQ7LRFTBAwjJRYmOhInGBkMFVc//aAAgBAQABPwD/ALUqtVmKSwHXRv3F2htsGwqI2AROV+qzhVvppTSD8DR3gA2xxub0t/zFRxub0t/zFRxuc0t/zFRxub0t/wAxUcbm9Lf8xUcbnNLf8xUcbm9Lf8xUcbnNLf8AMVHG5vS3/MVHG5vS3/MVHG5zS3/MVHG5vS3/ADFRxuc0t/zFRxub0t/zFRxub0t/zFRxuc0t/wAxUcbm9Lf8xUcbnNLf8xUcbm9Lf8xUcbm9Lf8AMVHG5zS3/MVHG5vS3/MVHG5zS3/MVHHJzS3/ADFRJ1+qSak72aU4gfA6Ssf55RFJqzFWYLrQ3jiLA42TaUk7QehaxPqqNQmJgqtb3xS0MgQOQa8J3A24RaEK1RwbmbVqjg3M2rVHBuZtWqODczatUcG5m1ao4NzNq1RwbmbVqjg3M2rVHBuZtWqODczatUcG5m1ao4NzNq1RwbmbVqjg3M2rVHBuZtWqODczatUcG5m1ao4NzNq1RwbmbVqjg3M2rVHBuZtWqODczatUFtwC0oVq3KRPrps+zMJV/oJ3jgyFB6EdJDTpGRBgYBFMaQ9UZFl1O+Qt9AUDlFuCAAAAB+nIBBBEVJtDNQnmm0hKEPuJSkYAAqDgMNkqbbUcJSCeg3uZd7CtkDAIo+Nad3hvb7Dz7EunfPvIbTgBWoJH+Y/HKR/9FjxQy+xMJ37DyHE/zQoH9HVsaVHvLv1GDgMM8y12E7Og3uZd7CtkDAIo+Nad3hvbu1uvClgykpvVTZFqiRaGwRth+YfmnC7MPLcWcqjbuNPOsOB1hxbbgwKQSDrEUC6Lj6hKTpCZiwlK8Ac6rMh/RVbGlR7y79Rg4DDPMtdhOzoN7mXewrZAwCKPjWnd4b27k7MiSlH5ki0NoJA/mcghxxx5xbrqipxaipSjlJ9hC1trQ42opWkhSSMIIinTqZ2Qlpu0EqbG/syLHIRr/Q1bGlR7y79Rg4DDPMtdhOzoN7mXewrZAwCKPjWnd4b27l1KiKM+Af4ltg+IH2rk3CKQQT/C+sJ1A/kVmssUhgAALmFj3bf7ldUUO6Z0PmXqbu+Q6u1LquQIJyH+nZ+RVsaVHvLv1GDgMM8y12E7Og3uZd7CtkDAIo+Nad3hvbuVaSVOUybl0C1akWpHWk74bPaosuZGlSbav41I36gchX/q5f8Ab26zWWKQwAAFzCx7tv8AcrqiZmHpt5yYmHCtxZtKjuXO3Q8W3khPL9xyJacNgDfUrq9urY0qPeXfqMHAYZ5lrsJ2dBvcy72FbIGARR8a07vDe3dujoDjTrk/JI3zS7VuNpwoOVQ6vYudoC5pxuenG7JZNikJPzCD9Pt1mssUhgAALmFj3bf7ldUTEy9NvOTEw4VuLNpJ9i526Ey5RIzznuTYltxR5vqP9Oz2qtjSo95d+owcBhnmWuwnZ0G9zLvYVsgYBFHxrTu8N7fYnLnKXPLLimS0s4VNEJt/3HKIvMlLeSeesyixMSlzNKlVBZZU6sG0F1W+/wACwe3WayxSGAAAuYWPdt/uV1RMzD0285MTDhW4s2lR9q566Ey+8kJ9y1nkS04oj3fUon4dns1bGlR7y79Rg4DDPMtdhOzoN7mXewrZAwCKPjWnd4b2/mVmssUhgAALmFj3bf7ldUTEy9NvOTEw4VuLNpJ/IuduhMuUSM857k2JbcUeb6j/AE7PYq2NKj3l36jBwGGeZa7CdnQb3Mu9hWyBgEUfGtO7w3t9qZmWJVlb76wltI5TEldcHJ5aJpsNyqyA2rKjtf7wCCAQd2s1likMAABcwse7b/crqiZmHpt5yYmHCtxZtKj+Vc9dCZfeSE+5azyJacUR7vqUT8Ozdq2NKj3l36jBwGGeZa7CdnQb3Mu9hWyBgEUfGtO7w3t9l99qWaW++sIbQLVKMVarPVN602oYSfdt/wDp69y566EyZRJTq7ZY8iFn5fV2YBBAIMVmssUhgAALmFj3bf7ldUTEy9NvOTEw4VuLNpJ/MuXdcdo8vwiyreqWgE5Eg8g3KtjSo95d+owcBhnmWuwnZ0G9zLvYVsgYBFHxrTu8N7fYmZliVZW++sJbSOUxWaw9Vn98bUMI5tu3B1nrPsUq6SYp0s5LLRwyQn3Np/gP8j/TEzMPTbzkxMOFbizaVH825ZQTRWj/AHHNu5VsaVHvLv1GDgMM8y12E7Og3uZd7CtkDAIo+Nad3hvbuzD7Uqw5MPK3raBaTFWqz1TetNqGEn3bf/p6/wBHcsoJorR/uObdyrY0qPeXfqMHAYZ5lrsJ2dBvcy72FbIGARR8a07vDe3cmH2pVhyYeVvW0C0mKtVnqm9abUMJPu2//T1/pLllBNFaP9xzbuVbGlR7y79Rg4DDPMtdhOzoN7mXewrZAwCKPjWnd4b27l1KQmjO2D5je39LcsoJorR/uObdyrY0qPeXfqMHAYZ5lrsJ2dBvcy72FbIGARR8a07vDe3cuqxO7/8Ao3t/S3LKCaK0f7jm3cq2NKj3l36jBwGGeZa7CdnQb3Mu9hWyBgEUfGtO7w3t3Lopd+bpjjMs2XHC4ixKeoxe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xe/WdAXyAHCn7xQJZ+UpTcvMtlte/WSk4bCdyrY0qPeXfqMHAYZ5lrsJ2dBvcy72FbIGARR8a07vDe39TVsaVHvLv1GDgMM8y12E7Og1JCkqScoIgpUglChYpJsIOQiJZ9crMMTKACppaVgHASkwLr6WoJK2JgHKN6k2f5i/Cl5qZ8CfVF+FLzUz4E+qL8KXmpnwJ9UX4UvNTPgT6ovwpeamfAn1RfhS81M+BPqi/Cl5qZ8CfVF+FLzUz4E+qL8KXmpnwJ9UX4UvNTPgT6ovwpeamfAn1RfhS81M+BPqi/Cl5qZ8CfVF+FLzUz4E+qL8KXmpnwJ9UX4UvNTPgT6ovwpeamfAn1RfhS81M+BPqi/Cl5qZ8CfVF+FLzUz4E+qL8KXmpnwJ9UX4UvNTPgT6oN2NM3psYmCcg3qfvEw8uZfemHAAt1ZWqzBaTbASpZCEi1SjYAMpMJSEpSkZAB0JdRSFsTC6iwglh02uWD+BeUnqP6u5qkOPvoqL6Slho2t5CtYyjqHQqkpUkpUAUkEEHlBBicuQkX1lyVeXLkm0pA36P8AgReUu3GQstzOTxReWuy01NGT5Wv4oNxa7CfxNOX5Wr4ovKXbjIWW5nJ4ovLXZaamjJ8rX8UG4tdhP4mnL8rV8UXlLtxkLLczk8UXlrstNTRk+Vr+KDcWuwn8TTl+Vq+KLyl24yFluZyeKLy12WmpoyfK1/FBuLXYT+Jpy/K1fFF5S7cZCy3M5PFF5a7LTU0ZPla/ig3FrsJ/E05flavii8pduMhZbmcnii8tdlpqaMnytfxQbi12E/iacvytXxReUu3GQstzOTxReWuy01NGT5Wv4oNxa7CfxNOX5Wr4ovKXbjIWW5nJ4ovLXZaamjJ8rX8UXlLy1IZfk6viiSuRkZde/mnVTJGBJG8R/wAgQlKUpCUgBIAAA5AAP+xP/8QAFBEBAAAAAAAAAAAAAAAAAAAAkP/aAAgBAgEBPwBAP//EABQRAQAAAAAAAAAAAAAAAAAAAJD/2gAIAQMBAT8AQD//2Q=="

/***/ }),

/***/ 2372:
/***/ (function(module) {

"use strict";
module.exports = require("@ant-design/icons");;

/***/ }),

/***/ 953:
/***/ (function(module) {

"use strict";
module.exports = require("antd");;

/***/ }),

/***/ 8417:
/***/ (function(module) {

"use strict";
module.exports = require("next/dist/next-server/lib/router-context.js");;

/***/ }),

/***/ 2238:
/***/ (function(module) {

"use strict";
module.exports = require("next/dist/next-server/lib/router/utils/get-asset-path-from-route.js");;

/***/ }),

/***/ 9297:
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ 79:
/***/ (function(module) {

"use strict";
module.exports = require("react-redux");;

/***/ }),

/***/ 7765:
/***/ (function(module) {

"use strict";
module.exports = require("redux-saga");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
return __webpack_require__.X([], 7770);
})();