module.exports =
(function() {
var exports = {};
exports.id = 888;
exports.ids = [888];
exports.modules = {

/***/ 7030:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "r": function() { return /* binding */ REDUX; },
/* harmony export */   "A": function() { return /* binding */ REDUX_SAGA; }
/* harmony export */ });
const REDUX = {
  AUTHEN: {
    FAIL_AUTHEN: "FAIL_AUTHEN",
    EXACT_AUTHEN: "EXACT_AUTHEN"
  },
  ANNOUNCEMENTS: {
    GET_ANNOUNCEMENTS: "GET_ANNOUNCEMENTS",
    GET_ANNOUNCEMENT_DETAIL: "GET_ANNOUNCEMENT_DETAIL"
  },
  EMPLOYER_DETAIL: {
    GET_EMPLOYER_DETAIL: "GET_EMPLOYER_DETAIL"
  },
  EMPLOYER_MORE_JOB: {
    GET_EMPLOYER_MORE_JOB: "EMPLOYER_MORE_JOB",
    SET_LOADING_MORE_JOB: "SET_LOADING_MORE_JOB"
  },
  SIMILAR_JOB: {
    GET_SIMILAR_JOB: "GET_SIMILAR_JOB",
    SET_LOADING_SIMILAR_JOB: "SET_LOADING_SIMILAR_JOB"
  },
  EVENT: {
    NOT_AVAILABLE: 'EVENT_IS_NOT_AVAILABLE',
    START: "CHECK_EVENT_STATUS",
    DETAIL: "GET_EVENT",
    LOGO_SCHOOL: "SET_LOGO_SCHOOL",
    LOADING: "SET_LOADING",
    JOB: {
      HOT: "GET_EVENT_HOT_JOBS",
      HOT_LOADING: "SET_LOADING_EVENT_HOT_JOB",
      HOT_CURRENT_PAGE: "SET_CURRENT_PAGE_EVENT_HOT_JOB",
      NORMAL: "GET_EVENT_NORMAL_JOBS",
      NORMAL_CURRENT_PAGE: "GET_EVENT_NORMAL_JOBS_CURRENT_PAGE",
      ALL: "GET_EVENT_ALL_JOBS",
      DETAIL: "GET_EVENT_JOB_DETAIL",
      SAVE: "SAVE_EVENT_JOB",
      BRANCH: 'GET_LIST_BRANCH_JOB',
      SEARCH: 'GET_JOBS_RESULTS'
    },
    EMPLOYER: {
      ALL: "GET_ALL_EMPLOYER",
      TOP: "GET_TOP_EMPLOYER",
      BANNER: 'GET_BANNER_EMPLOYER',
      MORE_JOB: 'GET_EMPLOYER_MORE_JOB'
    }
  },
  HIGH_LIGHT: {
    GET_HIGH_LIGHT_JOB: "GET_HIGH_LIGHT_JOB",
    SET_LOADING_HIGH_LIGHT_JOB: "SET_LOADING_HIGH_LIGHT_JOB"
  },
  IN_DAY: {
    GET_IN_DAY_JOB: "GET_IN_DAY_JOB"
  },
  HOT_JOB: {
    GET_HOT_JOB: "GET_HOT_JOB",
    SET_LOADING_HOT_JOB: "SET_LOADING_HOT_JOB"
  },
  ALL_JOB: {
    GET_ALL_JOB: "GET_ALL_JOB",
    SET_LOADING_ALL_JOB: "SET_LOADING_ALL_JOB"
  },
  JOB_DETAIL: {
    GET_JOB_DETAIL: "GET_JOB_DETAIL"
  },
  JOB_RESULT: {
    GET_JOB_RESULT: "GET_JOB_RESULT",
    SEARCH_JOB_DTO: "SEARCH_JOB_DTO",
    SET_FILTER_JOB_TYPE: "SET_FILTER_JOB_TYPE",
    SET_FILTER_LIST_SHIFT: "SET_FILTER_LIST_SHIFT",
    SET_FILTER_LIST_DAY: "SET_FILTER_LIST_DAY",
    SET_FILTER_AREA: "SET_FILTER_AREA",
    SET_FILTER_JOBNAME: "SET_FILTER_JOBNAME",
    SET_LOADING_RESULT: "SET_LOADING_RESULT",
    SET_FILTER: "SET_FILTER"
  },
  SAVED_JOB: {
    GET_SAVED_JOB: "GET_SAVED_JOB",
    SET_LOADING_SAVE_JOB: "SET_LOADING_SAVE_JOB"
  },
  HISTORY_APPLY: {
    GET_HISTORY_APPLY: "GET_HISTORY_APPLY",
    SET_LOADING_HISTORY_APPLY: "SET_LOADING_HISTORY_APPLY"
  },
  MAP: {
    SET_MAP_STATE: "SET_MAP_STATE"
  },
  MOBILE_STATE: {
    SET_MOBILE_STATE: "SET_MOBILE_STATE"
  },
  NOTI: {
    GET_NOTI: "GET_NOTI"
  },
  PERSON_INFO: {
    GET_SHORT_PERSON_INFO: "PERSONAL_INFO",
    GET_FULL_PERSON_INFO: 'FULL_INFO'
  },
  POPUP: {
    OPEN_POPUP: "OPEN_POPUP",
    CLOSE_POPUP: "CLOSE_POPUP"
  },
  SIDE_BAR: {
    OPEN_SIDE_BAR: "OPEN_SIDE_BAR",
    CLOSE_SIDE_BAR: "CLOSE_SIDE_BAR"
  },
  CHAT_ROOM: {
    SET_CHAT_ROOM: "SET_CHAT_ROOM"
  },
  JOB_NAMES: {
    GET_JOB_NAMES: "GET_JOB_NAMES"
  },
  JOB_GROUPS: {
    GET_JOB_GROUPS: "GET_JOB_GROUPS"
  },
  REGIONS: {
    GET_REGIONS: "GET_REGIONS"
  }
};
const REDUX_SAGA = {
  EMPLOYER_DETAIL: {
    GET_EMPLOYER_DETAIL: "GET_EMPLOYER_DETAIL_DATA"
  },
  EMPLOYER_MORE_JOB: {
    GET_EMPLOYER_MORE_JOB: "GET_EMPLOYER_MORE_JOB_DATA"
  },
  SIMILAR_JOB: {
    GET_SIMILAR_JOB: "GET_SIMILAR_JOB_DATA"
  },
  HIGH_LIGHT: {
    GET_HIGH_LIGHT_DATA: "GET_HIGH_LIGHT_DATA"
  },
  EVENT: {
    DETAIL: "GET_EVENT_DATA",
    JOB: {
      HOT: "GET_EVENT_HOT_JOBS_DATA",
      HOT_LOADING: "SET_LOADING_EVENT_HOT_JOB_DATA",
      NORMAL: "GET_EVENT_NORMAL_JOBS_DATA",
      ALL: "GET_EVENT_ALL_JOBS_DATA",
      DETAIL: "GET_EVENT_JOB_DETAIL_DATA",
      SAVE: "SAVE_EVENT_JOB_DATA",
      BRANCH: 'GET_LIST_BRANCH_JOB_DATA',
      SEARCH: 'GET_JOBS_RESULTS_DATA'
    },
    EMPLOYER: {
      ALL: "GET_ALL_EMPLOYER_DATA",
      TOP: "GET_TOP_EMPLOYER_DATA",
      BANNER: 'GET_BANNER_EMPLOYER_DATA',
      MORE_JOB: 'GET_EMPLOYER_MORE_JOB_DATA'
    }
  },
  IN_DAY: {
    GET_IN_DAY_JOB: "GET_IN_DAY_JOB_DATA"
  },
  HOT_JOB: {
    GET_HOT_JOB: "GET_HOT_JOB_DATA"
  },
  ALL_JOB: {
    GET_ALL_JOB: "GET_ALL_JOB_DATA"
  },
  JOB_DETAIL: {
    GET_JOB_DETAIL: "GET_JOB_DETAI_DATA"
  },
  JOB_RESULT: {
    GET_JOB_RESULT: "GET_JOB_RESULT_DATA"
  },
  SAVED_JOB: {
    GET_SAVED_JOB: "GET_SAVED_JOB_DATA"
  },
  HISTORY_APPLY: {
    GET_HISTORY_APPLY: "GET_HISTORY_APPLY_DATA"
  },
  NOTI: {
    GET_NOTI: "GET_NOTI_DATA"
  },
  PERSON_INFO: {
    GET_SHORT_PERSON_INFO: "PERSONAL_INFO_DATA",
    GET_FULL_PERSON_INFO: 'GET_FULL_INFO_DATA'
  },
  JOB_NAMES: {
    GET_JOB_NAMES: "GET_JOB_NAMES_DATA"
  },
  JOB_GROUPS: {
    GET_JOB_GROUPS: "GET_JOB_GROUPS_DATA"
  },
  REGIONS: {
    GET_REGIONS: "GET_REGIONS_DATA"
  },
  ANNOUNCEMENTS: {
    GET_ANNOUNCEMENTS: "GET_ANNOUNCEMENTS_DATA",
    GET_ANNOUNCEMENT_DETAIL: "GET_ANNOUNCEMENT_DETAIL_DATA"
  }
};

/***/ }),

/***/ 4151:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "U4": function() { return /* binding */ PUBLIC_HOST; }
/* harmony export */ });
/* unused harmony exports AUTH_HOST, STUDENT_HOST */
const AUTH_HOST = process.env.REACT_APP_API_HOST + '/oauth2';
const STUDENT_HOST = process.env.REACT_APP_API_HOST + '/students';
const PUBLIC_HOST = 'https://api.works.vn:8443' + '/public';

/***/ }),

/***/ 6627:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ _app; }
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);
// EXTERNAL MODULE: ./store.js + 14 modules
var store = __webpack_require__(1645);
// EXTERNAL MODULE: ./node_modules/antd/dist/antd.css
var antd = __webpack_require__(4722);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(79);
// EXTERNAL MODULE: ./const/actions.ts
var actions = __webpack_require__(7030);
;// CONCATENATED MODULE: external "next/router"
var router_namespaceObject = require("next/router");;
var router_default = /*#__PURE__*/__webpack_require__.n(router_namespaceObject);
// EXTERNAL MODULE: ./services/base-api.ts + 1 modules
var base_api = __webpack_require__(7001);
// EXTERNAL MODULE: ./environment/development.js
var development = __webpack_require__(4151);
// EXTERNAL MODULE: ./services/auth.ts + 1 modules
var auth = __webpack_require__(9032);
;// CONCATENATED MODULE: ./pages/_app.js

var __jsx = (external_react_default()).createElement;











function App({
  Component,
  pageProps
}) {
  const dispatch = (0,external_react_redux_.useDispatch)(); // const router = useRouter()

  const {
    0: loading,
    1: setLoading
  } = (0,external_react_.useState)(true);
  (0,external_react_.useEffect)(() => {
    if ('scrollRestoration' in window.history) {
      window.history.scrollRestoration = 'manual';
      let shouldScrollRestore;
      let cachedScrollPositions = [];
      router_default().events.on('routeChangeStart', () => {
        cachedScrollPositions.push([window.scrollX, window.scrollY]);
      });
      router_default().events.on('routeChangeComplete', () => {
        if (shouldScrollRestore) {
          const {
            x,
            y
          } = shouldScrollRestore;
          window.scrollTo(x, y);
          shouldScrollRestore = false;
        }
      });
      router_default().beforePopState(() => {
        const [x, y] = cachedScrollPositions.pop();
        shouldScrollRestore = {
          x,
          y
        };
        return true;
      });
    }
  }, []);
  (0,external_react_.useEffect)(() => {
    dispatch({
      type: actions/* REDUX_SAGA.EVENT.JOB.HOT */.A.EVENT.JOB.HOT
    }); // console.log(window.location.search)

    let searchParams = new URLSearchParams(window.location.search);
    let dataParams = searchParams.get("data");

    if (dataParams) {
      let decodeData = window.atob(dataParams);
      let newDataParams = new URLSearchParams(decodeData);
      let schoolID = newDataParams.get("schoolID");
      let eventID = newDataParams.get("eventID");
      console.log(schoolID);
      console.log(eventID);

      if (schoolID && eventID) {
        getInfoSchool(schoolID).then(res => {
          if (res && res.data) {
            // this.props.setInfoEvent(res.data.logoUrl, res.data.primaryColor, res.data.primaryDarkColor, window.location.search, queryParam2.schoolID, queryParam2.eventID, res.data.name)
            dispatch({
              type: actions/* REDUX.EVENT.LOGO_SCHOOL */.r.EVENT.LOGO_SCHOOL,
              res: res.data.logo,
              primaryColor: res.data.primaryColor,
              primaryDarkColor: res.data.primaryDarkColor,
              param: window.location.search,
              schoolID,
              eventID,
              name: res.data.name
            });
          }
        }).finally(() => {
          setLoading(false);
        });
      } else {
        setLoading(false);
      }
    } else {
      setLoading(false);
    }

    console.log(dataParams);
  }, []);

  function getInfoSchool(schoolID) {
    let res = (0,base_api/* _get */.mR)(null, `/api/schools/${schoolID}/simple`, development/* PUBLIC_HOST */.U4, auth/* noInfoHeader */.W);

    return res;
  }

  return __jsx(Component, pageProps);
}

/* harmony default export */ var _app = (store/* wrapper.withRedux */.YS.withRedux(App)); // initRouterListeners();
// function initRouterListeners() {
//     console.log("Init router listeners");
//     const routes = [];
//     Router.events.on('routeChangeStart', (url) => {
//         pushCurrentRouteInfo();
//     });
//     Router.events.on('routeChangeComplete', (url) => {
//         fixScrollPosition();
//     });
//     // Hack to set scrollTop because of this issue:
//     // - https://github.com/zeit/next.js/issues/1309
//     // - https://github.com/zeit/next.js/issues/3303
//     function pushCurrentRouteInfo() {
//         routes.push({pathname: Router.pathname, scrollY: window.scrollY});
//     }
//     // TODO: We guess we're going back, but there must be a better way
//     // https://github.com/zeit/next.js/issues/1309#issuecomment-435057091
//     function isBack() {
//         return routes.length >= 2 && Router.pathname === routes[routes.length - 2].pathname;
//     }
//     function fixScrollPosition () {
//         let scrollY = 0;
//         if (isBack()) {
//             routes.pop(); // route where we come from
//             const targetRoute = routes.pop(); // route where we return
//             scrollY = targetRoute.scrollY; // scrollY we had before
//         }
//         console.log("Scrolling to", scrollY);
//         window.requestAnimationFrame(() => window.scrollTo(0, scrollY));
//         console.log("routes now:", routes);
//     }
// }

/***/ }),

/***/ 9032:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "$": function() { return /* binding */ authHeaders; },
  "W": function() { return /* binding */ noInfoHeader; }
});

;// CONCATENATED MODULE: ./const/check-server.ts
const ISSERVER = true;
;// CONCATENATED MODULE: ./services/auth.ts
 // No info header

const noInfoHeader = {
  'Access-Control-Allow-Headers': '*',
  "Content-Type": "application/json"
}; // Check invalid header

const authHeaders = {
  'Access-Control-Allow-Headers': '*',
  'Authorization': `Bearer ${!ISSERVER ? localStorage.getItem("accessToken") : null}`
};

/***/ }),

/***/ 7001:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "ZJ": function() { return /* binding */ _delete; },
  "mR": function() { return /* binding */ _get; },
  "d8": function() { return /* binding */ _post; },
  "SQ": function() { return /* binding */ _put; }
});

;// CONCATENATED MODULE: external "axios"
var external_axios_namespaceObject = require("axios");;
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_namespaceObject);
// EXTERNAL MODULE: ./services/auth.ts + 1 modules
var auth = __webpack_require__(9032);
;// CONCATENATED MODULE: ./services/base-api.ts

 // POST

const _post = async (data, api, another_host, headers, params) => {
  let requestURL = (another_host ? another_host : process.env.REACT_APP_API_HOST) + api;

  if (headers === null || headers === undefined) {
    headers = auth/* authHeaders */.$;
  }

  let response = await external_axios_default().post(requestURL, data, {
    headers,
    params
  });
  return response.data;
}; //GET

const _get = async (params, api, another_host, headers) => {
  let requestURL = (another_host ? another_host : process.env.REACT_APP_API_HOST) + api;

  if (headers === null || headers === undefined) {
    headers = auth/* authHeaders */.$;
  }

  let response = await external_axios_default().get(requestURL, {
    params: params,
    headers
  });
  return response.data;
}; // DELETE

const _delete = async (data, api, another_host, headers, params) => {
  let requestURL = (another_host ? another_host : process.env.REACT_APP_API_HOST) + api;

  if (headers === null || headers === undefined) {
    headers = auth/* authHeaders */.$;
  }

  let response = await external_axios_default().delete(requestURL, {
    data: params,
    headers
  });
  return response.data;
}; // PUT

const _put = async (data, api, another_host, headers, params) => {
  let requestURL = (another_host ? another_host : process.env.REACT_APP_API_HOST) + api;

  if (headers === null || headers === undefined) {
    headers = auth/* authHeaders */.$;
  }

  let response = await external_axios_default().put(requestURL, data, {
    headers,
    params
  });
  return response.data;
};

/***/ }),

/***/ 1645:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "YS": function() { return /* binding */ wrapper; }
});

// UNUSED EXPORTS: makeStore, store

;// CONCATENATED MODULE: external "redux"
var external_redux_namespaceObject = require("redux");;
// EXTERNAL MODULE: external "redux-saga"
var external_redux_saga_ = __webpack_require__(7765);
var external_redux_saga_default = /*#__PURE__*/__webpack_require__.n(external_redux_saga_);
;// CONCATENATED MODULE: external "next-redux-wrapper"
var external_next_redux_wrapper_namespaceObject = require("next-redux-wrapper");;
// EXTERNAL MODULE: ./const/actions.ts
var actions = __webpack_require__(7030);
;// CONCATENATED MODULE: ./redux/reducers/event/job.ts
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


let initListEventHotJob = {
  data: {
    pageIndex: 0,
    pageSize: 0,
    totalItems: 0,
    items: []
  },
  loading: true,
  currentPage: 1
};
const EventHotJobResults = (state = initListEventHotJob, action) => {
  switch (action.type) {
    case actions/* REDUX.EVENT.JOB.HOT */.r.EVENT.JOB.HOT:
      return _objectSpread(_objectSpread({}, state), {}, {
        data: action.data
      });

    case actions/* REDUX.EVENT.JOB.HOT_CURRENT_PAGE */.r.EVENT.JOB.HOT_CURRENT_PAGE:
      return _objectSpread(_objectSpread({}, state), {}, {
        currentPage: action.currentPage
      });

    case actions/* REDUX.EVENT.JOB.HOT_LOADING */.r.EVENT.JOB.HOT_LOADING:
      return _objectSpread(_objectSpread({}, state), {}, {
        loading: action.loading
      });

    default:
      return _objectSpread({}, state);
  }
};
;// CONCATENATED MODULE: ./redux/reducers/event/detail.ts
function detail_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function detail_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { detail_ownKeys(Object(source), true).forEach(function (key) { detail_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { detail_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function detail_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


let initListDetailEvent = {
  logo: null,
  primaryColor: '#1890ff',
  primaryDarkColor: '#296ea7',
  param: '',
  schoolID: '',
  eventID: '',
  name: ''
};
const DetailEvent = (state = initListDetailEvent, action) => {
  switch (action.type) {
    case actions/* REDUX.EVENT.LOGO_SCHOOL */.r.EVENT.LOGO_SCHOOL:
      return detail_objectSpread(detail_objectSpread({}, state), {}, {
        logo: action.logo,
        primaryColor: action.primaryColor,
        primaryDarkColor: action.primaryDarkColor,
        param: action.param,
        schoolID: action.schoolID,
        eventID: action.eventID,
        name: action.name
      });

    default:
      return detail_objectSpread({}, state);
  }
};
;// CONCATENATED MODULE: ./redux/reducers/auth.ts
function auth_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function auth_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { auth_ownKeys(Object(source), true).forEach(function (key) { auth_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { auth_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function auth_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


let initAuth = {
  isAuthen: false
};
const AuthState = (state = initAuth, action) => {
  switch (action.type) {
    case actions/* REDUX.AUTHEN.EXACT_AUTHEN */.r.AUTHEN.EXACT_AUTHEN:
      return auth_objectSpread(auth_objectSpread({}, state), {}, {
        isAuthen: true
      });

    case actions/* REDUX.AUTHEN.FAIL_AUTHEN */.r.AUTHEN.FAIL_AUTHEN:
      return auth_objectSpread(auth_objectSpread({}, state), {}, {
        isAuthen: false
      });

    default:
      return state;
  }
};
;// CONCATENATED MODULE: ./redux/store/reducer.ts
function reducer_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function reducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { reducer_ownKeys(Object(source), true).forEach(function (key) { reducer_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { reducer_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function reducer_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






let rootReducer = {
  EventHotJobResults: EventHotJobResults,
  DetailEvent: DetailEvent,
  AuthState: AuthState
};
const myReducer = (0,external_redux_namespaceObject.combineReducers)(rootReducer);

const newReducer = (state, action) => {
  if (action.type === external_next_redux_wrapper_namespaceObject.HYDRATE) {
    const nextState = reducer_objectSpread(reducer_objectSpread({}, state), action.payload); // if (state.faqCategories && state.faqCategories.categories.length > 0)
    //   nextState.faqCategories = state.faqCategories; // preserve categories on client side navigation


    return nextState;
  } else {
    return myReducer(state, action);
  }
};

/* harmony default export */ var reducer = (newReducer);
;// CONCATENATED MODULE: external "redux-saga/effects"
var effects_namespaceObject = require("redux-saga/effects");;
;// CONCATENATED MODULE: ./const/method.ts
const GET = 'GET';
const POST = 'POST';
const PUT = 'PUT';
const DELETE = 'DELETE';
// EXTERNAL MODULE: ./services/base-api.ts + 1 modules
var base_api = __webpack_require__(7001);
;// CONCATENATED MODULE: external "sweetalert"
var external_sweetalert_namespaceObject = require("sweetalert");;
var external_sweetalert_default = /*#__PURE__*/__webpack_require__.n(external_sweetalert_namespaceObject);
;// CONCATENATED MODULE: ./const/type.ts
const TYPE = {
  SUCCESS: "success",
  ERROR: "error",
  WARNING: "warning",
  FULLTIME: "FULLTIME",
  PARTTIME: "PARTTIME",
  INTERNSHIP: "INTERNSHIP",
  MON: 'MON',
  TUE: 'TUE',
  WED: 'WED',
  THU: 'THU',
  FRI: 'FRI',
  SAT: 'SAT',
  SUN: 'SUN',
  MOR: 'MOR',
  AFT: 'AFT',
  EVN: 'EVN',
  TOP: 'TOP',
  HIGHLIGHT: 'HIGHLIGHT',
  EVENT: 'EVENT',
  NORMAL: 'NORMAL'
};
;// CONCATENATED MODULE: ./services/exception.ts


const exceptionShowNoti = async (err, hide_alert_error) => {
  // console.log(err);
  if (err && err && err.response && err.response.data && !hide_alert_error) {
    let msg = err.response.data.msg;
    external_sweetalert_default()({
      title: "Worksvn thông báo",
      text: msg,
      icon: TYPE.ERROR,
      dangerMode: true
    });
  }
};
;// CONCATENATED MODULE: ./services/exec.ts





const _requestToServer = async (method, data, api, host, headers, params, show_alert, log_query, hide_alert_error) => {
  let res;

  try {
    switch (method) {
      case GET:
        res = await (0,base_api/* _get */.mR)(params, api, host, headers);
        break;

      case POST:
        res = await (0,base_api/* _post */.d8)(data, api, host, headers, params);

        if (show_alert) {
          external_sweetalert_default()({
            title: "Worksvn thông báo",
            text: `${res.msg}`,
            icon: TYPE.SUCCESS,
            dangerMode: false
          });
        }

        break;

      case PUT:
        res = await (0,base_api/* _put */.SQ)(data, api, host, headers, params);

        if (show_alert) {
          external_sweetalert_default()({
            title: "Worksvn thông báo",
            text: `Cập nhập ${res.msg}`,
            icon: TYPE.SUCCESS,
            dangerMode: false
          });
        }

        return res;

      case DELETE:
        res = await (0,base_api/* _delete */.ZJ)(data, api, host, headers, params);
        break;

      default:
        break;
    }

    if (show_alert && res) {
      external_sweetalert_default()({
        title: "worksvn thông báo",
        text: res.msg,
        icon: TYPE.SUCCESS,
        dangerMode: false
      });
    }

    if (log_query) {// console.log(host + api);
      // console.log(params);
      // console.log(data);
      // console.log(res);
    }
  } catch (err) {
    console.log(err);
    console.log(err.response && err.response.data);
    exceptionShowNoti(err, hide_alert_error);
  }

  return res;
};
// EXTERNAL MODULE: ./services/auth.ts + 1 modules
var auth = __webpack_require__(9032);
// EXTERNAL MODULE: ./environment/development.js
var development = __webpack_require__(4151);
;// CONCATENATED MODULE: ./redux/watcher/event/job.ts







function* getListHotJobData(action) {
  // console.log('vao getListHotJobData')
  yield (0,effects_namespaceObject.put)({
    type: actions/* REDUX.EVENT.JOB.HOT_LOADING */.r.EVENT.JOB.HOT_LOADING,
    loading: true
  });
  let res = yield (0,effects_namespaceObject.call)(getHotJobData, action);

  if (res) {
    let data = res.data;
    yield (0,effects_namespaceObject.put)({
      type: actions/* REDUX.EVENT.JOB.HOT */.r.EVENT.JOB.HOT,
      data
    });
  }

  yield (0,effects_namespaceObject.put)({
    type: actions/* REDUX.EVENT.JOB.HOT_LOADING */.r.EVENT.JOB.HOT_LOADING,
    loading: false
  });
}

function getHotJobData(action) {
  let res = _requestToServer(POST, {}, `/api/jobs/active`, development/* PUBLIC_HOST */.U4, auth/* noInfoHeader */.W, {
    pageIndex: action.pageIndex ? action.pageIndex : 0,
    pageSize: action.pageSize ? action.pageSize : 21,
    priority: 'TOP'
  }, false);

  return res;
}

function* EventHotJobWatcher() {
  yield (0,effects_namespaceObject.takeEvery)(actions/* REDUX_SAGA.EVENT.JOB.HOT */.A.EVENT.JOB.HOT, getListHotJobData);
}
;// CONCATENATED MODULE: ./redux/store/saga.ts


function* rootSaga() {
  try {
    yield (0,effects_namespaceObject.all)([EventHotJobWatcher()]);
  } catch (err) {
    console.log(err);
    throw err;
  }
}
;// CONCATENATED MODULE: ./store.js






const bindMiddleware = middleware => {
  // if (process.env.NODE_ENV !== 'production') {
  //   const { composeWithDevTools } = require('redux-devtools-extension')
  //   return composeWithDevTools(applyMiddleware(...middleware))
  // }
  return (0,external_redux_namespaceObject.applyMiddleware)(...middleware);
};

const sagaMiddleware = external_redux_saga_default()();
const store = (0,external_redux_namespaceObject.createStore)(reducer, bindMiddleware([sagaMiddleware]));
const makeStore = context => {
  store.sagaTask = sagaMiddleware.run(rootSaga);
  return store;
};
const wrapper = (0,external_next_redux_wrapper_namespaceObject.createWrapper)(makeStore, {
  debug: true
});

/***/ }),

/***/ 4722:
/***/ (function() {



/***/ }),

/***/ 9297:
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ 79:
/***/ (function(module) {

"use strict";
module.exports = require("react-redux");;

/***/ }),

/***/ 7765:
/***/ (function(module) {

"use strict";
module.exports = require("redux-saga");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
return __webpack_require__.X([], 6627);
})();