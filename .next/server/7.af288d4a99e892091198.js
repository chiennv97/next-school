exports.ids = [7];
exports.modules = {

/***/ "3WKp":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export getNumberWithDot */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return convertFullSalary; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return convertSalary; });
const getNumberWithDot = x => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
const convertFullSalary = (min, minUnit, max, maxUnit) => {
  let result = '';

  if (min) {
    result += getNumberWithDot(min);

    if (minUnit !== maxUnit) {
      result += '/' + minUnit;
    }
  }

  if (max) {
    if (result) {
      result += ' - ';
    }

    result += getNumberWithDot(max);

    if (maxUnit) {
      result += '/' + maxUnit;
    }
  }

  if (!result) {
    result = 'Thỏa thuận';
  }

  return result;
};
const convertSalary = (min, max, unit) => {
  return convertFullSalary(min, null, max, unit);
};

/***/ }),

/***/ "Amjv":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return JobType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IptLetter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return NotUpdate; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function JobType(props) {
  let style = {
    color: 'black',
    backgroundColor: 'white',
    fontSize: props.fontSize ? props.fontSize : '0.8em',
    textAlign: 'center',
    width: props.width ? props.width : '75px',
    display: 'inline-block',
    borderRadius: '5px'
  };
  let label;

  switch (props.children) {
    case 'FULLTIME':
      style.color = 'white';
      style.backgroundColor = '#06bbe4';
      label = 'FULL-TIME';
      break;

    case 'PARTTIME':
      style.color = 'white';
      style.backgroundColor = '#00b33c';
      label = 'PART-TIME';
      break;

    case 'INTERNSHIP':
      style.color = 'white';
      style.backgroundColor = '#ff9933';
      label = 'THỰC TẬP';
      break;

    default:
      break;
  }

  return __jsx("div", {
    style: style
  }, label);
}
const IptLetter = props => {
  return __jsx("span", {
    className: "important-letter"
  }, " " + props.value + " ");
};
const NotUpdate = props => {
  const {
    msg
  } = props;
  return __jsx("span", {
    style: {
      fontSize: '0.8 rem',
      fontStyle: 'italic'
    }
  }, msg ? msg : 'Chưa cập nhật');
};

/***/ }),

/***/ "Ttrx":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, "_checkGender", function() { return /* binding */ _checkGender; });
__webpack_require__.d(__webpack_exports__, "default", function() { return /* binding */ JobProperties_JobProperties; });

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "antd"
var external_antd_ = __webpack_require__("Exp3");

// EXTERNAL MODULE: external "@ant-design/icons"
var icons_ = __webpack_require__("nZwT");

// EXTERNAL MODULE: ./components/layout/common/Common.tsx
var Common = __webpack_require__("Amjv");

// EXTERNAL MODULE: external "moment"
var external_moment_ = __webpack_require__("wy2R");
var external_moment_default = /*#__PURE__*/__webpack_require__.n(external_moment_);

// CONCATENATED MODULE: ./utils/day.ts
const weekDays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
// EXTERNAL MODULE: ./utils/convertNumber.js
var convertNumber = __webpack_require__("3WKp");

// CONCATENATED MODULE: ./components/job/job-detail/job-properties/JobProperties.tsx
var __jsx = external_react_default.a.createElement;





 // import { _checkGender } from './../JobDetail';
// import './JobProperties.scss'


const _checkGender = data => {
  if (data.gender) {
    switch (data.gender) {
      case "BOTH":
        return __jsx("div", null, __jsx("p", null, __jsx("label", {
          style: {
            marginBottom: 0,
            marginRight: 3
          }
        }, __jsx(icons_["ManOutlined"], {
          style: {
            color: "rgb(21, 148, 255)"
          }
        }), "Nam &", " "), __jsx("label", {
          style: {
            marginBottom: 0,
            marginRight: 5
          }
        }, __jsx(icons_["WomanOutlined"], {
          style: {
            color: "#ff395c"
          }
        }), "N\u1EEF", " ")), __jsx("p", null, "S\u1ED1 l\u01B0\u1EE3t \u0111\xE3 \u1EE9ng tuy\u1EC3n: ", data.applied, "/", data.quantity));

      case "MALE":
        return __jsx("div", null, __jsx("p", null, __jsx("label", {
          style: {
            marginBottom: 0
          }
        }, __jsx(icons_["ManOutlined"], {
          style: {
            color: "#168ECD"
          }
        })), " ", "Nam -", __jsx("label", {
          style: {
            marginLeft: 5,
            marginBottom: 0
          }
        }, "S\u1ED1 l\u01B0\u1EE3t \u0111\xE3 \u1EE9ng tuy\u1EC3n: ", data.applied, "/", data.quantity)));

      case "FEMALE":
        return __jsx("div", null, __jsx("p", null, __jsx("label", {
          style: {
            marginBottom: 0
          }
        }, __jsx(icons_["WomanOutlined"], {
          style: {
            color: "#ff395c"
          }
        })), " ", "N\u1EEF -", __jsx("label", {
          style: {
            marginLeft: 5,
            marginBottom: 0
          }
        }, "S\u1ED1 l\u01B0\u1EE3t \u0111\xE3 \u1EE9ng tuy\u1EC3n: ", data.applied, "/", data.quantity)));

      default:
        return __jsx(Common["c" /* NotUpdate */], null);
    }
  }
};
class JobProperties_JobProperties extends external_react_["PureComponent"] {
  render() {
    let {
      jobDetail,
      similarJob,
      is_loading_similar,
      paging
    } = this.props;
    return __jsx("div", {
      className: "job-detail"
    }, __jsx(external_antd_["Row"], {
      className: "des-job",
      style: {
        backgroundColor: 'white'
      }
    }, __jsx(external_antd_["Col"], {
      xs: 24,
      sm: 24,
      md: 24,
      lg: 15,
      xl: 15
    }, __jsx("div", {
      className: "description-job "
    }, __jsx("h6", null, "Th\xF4ng tin c\xF4ng vi\u1EC7c"), __jsx("div", {
      style: {
        padding: 10,
        whiteSpace: 'pre-line',
        color: '#000'
      },
      dangerouslySetInnerHTML: {
        __html: jobDetail && jobDetail.description
      }
    }))), __jsx(external_antd_["Col"], {
      xs: 24,
      sm: 24,
      md: 24,
      lg: 9,
      xl: 9
    }, __jsx("div", {
      className: "short-info"
    }, __jsx("h6", null, "Th\xF4ng tin s\u01A1 l\u01B0\u1EE3c"), __jsx("ul", {
      style: {
        marginBottom: '5px'
      }
    }, __jsx("li", {
      className: "d_j_t"
    }, __jsx(icons_["CalendarOutlined"], {
      style: {
        color: 'rgb(74, 74, 74)'
      }
    }), __jsx(Common["a" /* IptLetter */], {
      value: "Ngày đăng: "
    }), __jsx("label", {
      style: {
        fontSize: '0.95em',
        color: '#000'
      }
    }, " ", jobDetail && external_moment_default()(jobDetail.createdDate).format('DD/MM/YYYY'))), __jsx("li", {
      className: "d_j_t"
    }, __jsx(icons_["CalendarOutlined"], {
      style: {
        color: 'rgb(74, 74, 74)'
      }
    }), __jsx(Common["a" /* IptLetter */], {
      value: "Ngày hết hạn: "
    }), __jsx("label", {
      style: {
        fontSize: '0.95em',
        color: '#000'
      }
    }, " ", jobDetail && external_moment_default()(jobDetail.expirationDate).format('DD/MM/YYYY'))))))), __jsx(external_antd_["Row"], null, __jsx(external_antd_["Col"], {
      xs: 24,
      sm: 24,
      md: 24,
      lg: 24,
      xl: 24
    }, __jsx(external_antd_["Row"], {
      className: "time-job "
    }, __jsx("h6", null, "Ca l\xE0m vi\u1EC7c"), jobDetail.shifts && jobDetail.shifts ? jobDetail.shifts.map((item, index) => {
      let maxSalary = '' + item.maxSalary && item.maxSalary === 0 ? '' : '-' + item.maxSalary;
      return __jsx(external_antd_["Col"], {
        key: index,
        xs: 24,
        sm: 24,
        md: 12,
        lg: 12,
        xl: 12,
        xxl: 12
      }, __jsx("div", {
        className: "time-content",
        style: {
          border: '1px solid rgb(119, 197, 255)',
          borderRadius: '3px'
        }
      }, __jsx("p", {
        style: {
          color: 'black',
          fontSize: '1.1em',
          borderBottom: '1px solid rgb(119, 197, 255)',
          paddingBottom: 5,
          paddingLeft: '15px',
          backgroundColor: 'rgb(226, 242, 254)',
          margin: '0',
          paddingTop: 5
        }
      }, "Ca s\u1ED1 ", index + 1), __jsx("p", null, __jsx(icons_["ClockCircleOutlined"], {
        style: {
          color: '#168ECD'
        }
      }), ' ' + item.startTime + '-' + item.endTime), __jsx("p", null, __jsx(icons_["DollarCircleOutlined"], {
        style: {
          color: '#168ECD'
        }
      }), Object(convertNumber["b" /* convertSalary */])(item.minSalary, item.maxSalary, item.unit)), __jsx("div", {
        className: "week-day"
      }, weekDays.map((itemWeek, index) => {
        if (item[itemWeek] === true) {
          let day = 'T' + (index + 2);

          if (index === 6) {
            day = 'CN';
          }

          return __jsx("label", {
            key: index,
            className: "time-span"
          }, day);
        } else {
          let day = 'T' + (index + 2);

          if (index === 6) {
            day = 'CN';
          }

          return __jsx("label", {
            key: index,
            className: "time-span-unselected"
          }, day);
        }
      })), item.genderRequireds[0] ? _checkGender(item.genderRequireds[0]) : null, item.genderRequireds[1] ? _checkGender(item.genderRequireds[1]) : null));
    }) : __jsx(Common["c" /* NotUpdate */], {
      msg: 'Ca làm việc không tồn tại'
    })))), __jsx(external_antd_["Row"], null, __jsx(external_antd_["Col"], {
      xs: 24,
      sm: 24,
      md: 24,
      lg: 24,
      xl: 24
    }, __jsx("div", {
      className: "skills-job-detail "
    }, __jsx("h6", null, "K\u0129 n\u0103ng kh\xE1c"), __jsx("div", {
      style: {
        padding: 15
      }
    }, jobDetail.requiredSkills && jobDetail.requiredSkills.length > 0 ? jobDetail.requiredSkills.map((item, index) => {
      return __jsx("label", {
        key: index,
        className: "skills-detail"
      }, item.name);
    }) : __jsx(Common["c" /* NotUpdate */], {
      msg: "\u1EE8ng vi\xEAn kh\xF4ng \u0111\xF2i h\u1ECFi k\u1EF9 n\u0103ng kh\xE1c"
    }))))));
  }

}

/***/ })

};;