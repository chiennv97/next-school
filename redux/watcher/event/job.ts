
import { takeEvery, put, call } from 'redux-saga/effects';
import { REDUX_SAGA, REDUX } from '../../../const/actions'
import {_requestToServer} from '../../../services/exec'
import { POST } from '../../../const/method';
import { noInfoHeader } from '../../../services/auth';
import { PUBLIC_HOST } from '../../../environment/development';

function* getListHotJobData(action) {
    // console.log('vao getListHotJobData')
    yield put({ type: REDUX.EVENT.JOB.HOT_LOADING, loading: true });
    let res = yield call(getHotJobData, action);
    if (res) {
        let data = res.data;
        yield put({ type: REDUX.EVENT.JOB.HOT, data });
    }
    yield put({ type: REDUX.EVENT.JOB.HOT_LOADING, loading: false });
}

function getHotJobData(action) {
    let res = _requestToServer(
        POST,
        {},
        `/api/jobs/active`,
        PUBLIC_HOST, noInfoHeader,
        {
            pageIndex: action.pageIndex ? action.pageIndex : 0,
            pageSize: action.pageSize? action.pageSize: 21,
            priority: 'TOP'
        },
        false
    );
    return res
}
export function* EventHotJobWatcher() {
    yield takeEvery(REDUX_SAGA.EVENT.JOB.HOT, getListHotJobData)
}
