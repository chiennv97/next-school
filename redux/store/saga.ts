

import { all } from 'redux-saga/effects';

import { EventHotJobWatcher } from "../watcher/event/job";


export default function* rootSaga() {
    try {
        yield all([
            EventHotJobWatcher()
        ])
    } catch (err) {
        console.log(err)
        throw err
    }
  
} 