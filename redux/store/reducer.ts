import { combineReducers } from 'redux';
import {EventHotJobResults} from '../reducers/event/job'
import {DetailEvent} from '../reducers/event/detail'
import { HYDRATE } from "next-redux-wrapper";
import {AuthState} from '../reducers/auth'
let rootReducer = {
    EventHotJobResults,
    DetailEvent,
    AuthState
  };
  
  const myReducer = combineReducers(rootReducer);

  const newReducer = (state, action) => {
    if (action.type === HYDRATE) {
      const nextState = {
        ...state, // use previous state
        ...action.payload, // apply delta from hydration
      };
      // if (state.faqCategories && state.faqCategories.categories.length > 0)
      //   nextState.faqCategories = state.faqCategories; // preserve categories on client side navigation
      return nextState;
    } else {
      return myReducer(state, action);
    }
  };
  
  export default newReducer;