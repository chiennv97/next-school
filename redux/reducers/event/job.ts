import { REDUX } from "../../../const/actions";
import { HYDRATE } from 'next-redux-wrapper';
let initListEventHotJob = {
  data: {
    pageIndex: 0,
    pageSize: 0,
    totalItems: 0,
    items: [],
  },
  loading: true,
  currentPage: 1
};

export const EventHotJobResults = (state = initListEventHotJob, action) => {
  switch (action.type) {
    case REDUX.EVENT.JOB.HOT:
      return { ...state, data: action.data };
    case REDUX.EVENT.JOB.HOT_CURRENT_PAGE:
      return { ...state, currentPage: action.currentPage }
    case REDUX.EVENT.JOB.HOT_LOADING:
      return { ...state, loading: action.loading }
    default:
      return { ...state };
  }
};
