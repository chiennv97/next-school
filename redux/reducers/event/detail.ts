
import { REDUX } from "../../../const/actions";

let initListDetailEvent = {
    logo: null,
    primaryColor: '#1890ff',
    primaryDarkColor: '#296ea7',
    param: '',
    schoolID: '',
    eventID: '',
    name: ''
};


export const DetailEvent = (state = initListDetailEvent, action) => {
    switch (action.type) {
        case REDUX.EVENT.LOGO_SCHOOL:
            return {
                ...state,
                logo: action.logo,
                primaryColor: action.primaryColor,
                primaryDarkColor: action.primaryDarkColor,
                param: action.param,
                schoolID: action.schoolID,
                eventID: action.eventID,
                name: action.name
            }
        default:
            return { ...state };
    }
};
