import styles from './Banner.module.scss';
import { Carousel } from 'antd';
import playgame from "../../assets/image/CHOINGAY.png";

export default function Banner() {
    const props = {
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 400,
      };
    return (
        <>
            <div
                className="employer-banner"
                style={{ width: '100%', position: 'relative' }}
            >
                <Carousel dots={true} {...props} autoplay >
                    <div className={styles.banner}>
                        <div className={styles.wapperButtonPlayGame}>
                            <img className={styles.buttonPlayGame} src={playgame} />
                        </div>

                        <img
                            src={'https://storage.googleapis.com/worksvn-prod.appspot.com/temp/LEKYKET_tlu.png'}
                            key={2}
                            alt="banner"
                            className={styles.imageBanner}
                        />
                    </div>
                    <div className={styles.banner}>
                        <img
                            src={'https://storage.googleapis.com/worksvn-prod.appspot.com/school_events/employer_banners/74d57629-3b0a-472f-9ab3-284987ce72e6/banner_1610351469285'}
                            key={1}
                            alt="banner"
                            className={styles.imageBanner}
                        />
                    </div>

                </Carousel>
            </div>
        </>
    )
}