import React, { PureComponent } from "react";
import { Col, Row, Pagination } from "antd";
import styles from "./TopJobEm.module.scss";
import { connect } from "react-redux";
// import { Link } from "react-router-dom";
import Link from 'next/link';

import { convertFullSalary } from '../../utils/convertNumber'
//@ts-ignore
import DefaultImage from "../../assets/image/base-image.jpg";
import { REDUX_SAGA, REDUX } from "../../const/actions";
import { JobType } from "../layout/common/Common";
import { EnvironmentOutlined, DollarOutlined } from '@ant-design/icons'

interface IProps {
    getEventHotJob?: Function;
    getInDay?: Function;
    topJob?: any;
    indayJob?: any;
    setLoadinggetEventHotJob?: Function;
    loading_hot_job?: any;
    param?: any;
    setCurrentPage?: Function;
    currentPage?: number;
}
interface IState {
    current: number;
}


class TopJob extends PureComponent<IProps, IState> {
    constructor(props) {
        super(props);
        this.state = {
            current: 1,
        };
    }
    componentDidMount() {
        console.log('vao componentDidMount')
        // this.props.getEventHotJob(0, 10);
    }
    changePage = (event?: number) => {
        this.props.getEventHotJob(event - 1, 10);
        // this.setState({
        //     current: event,
        // });
        this.props.setCurrentPage(event)

        // window.location.assign('#id_schedule_b')
    };


    render() {
        let { topJob, param, currentPage } = this.props;
        console.log(currentPage)
        return (
            <Row
                className={styles.homeJob}
                style={{ display: topJob.totalItems === 0 ? "none" : "" }}
                id="homeJob"
            >

                {topJob && topJob.items
                    ? topJob.items.map((item, index) => {
                        let logoUrl = item.employerLogoUrl;

                        if (!logoUrl) {
                            logoUrl = DefaultImage;
                        }

                        return (
                            <Col xs={24} sm={24} md={12} lg={12} xl={12} xxl={12} key={index}>
                                <div key={index} className={styles.hJItemTop} onClick={() => {
                                    // window.open(`/chi-tiet-cong-viec/${window.btoa(item.id)}${param}`)
                                }}>
                                    <div className={styles.imgJob}>
                                        <img src={logoUrl} alt="employer logo" />
                                        <JobType>{item.jobType}</JobType>
                                    </div>
                                    <div>

                                        <div className={styles.jobContent}>
                                            <ul>
                                                <li className={styles.jD}>
                                                    <Link
                                                        // to={`/chi-tiet-cong-viec/${window.btoa(item.id)}${param}`}
                                                        // target="_blank"
                                                        href="/chi-tiet-cong-viec/1"

                                                    >
                                                        <h6
                                                            className={styles.lC}
                                                            style={{
                                                                color: item.titleHighlight ? "red" : "black",
                                                            }}
                                                        >
                                                            <span className={styles.topBadge} style={{ marginRight: 5 }}>Hot</span>
                                                            {item.jobTitle}
                                                        </h6>
                                                    </Link>
                                                </li>
                                                <li className={styles.lC}>
                                                    <Link href="/about">
                                                        <a onClick={() => {
                                                            if (window.innerWidth > 768) {
                                                                // window.open(`/employer/${window.btoa(item.employerID)}${param}`)
                                                            }
                                                        }} className={styles.aNameEmployer}
                                                            style={{ textTransform: 'capitalize', fontSize: '0.8em' }}>
                                                            {item.employerName}
                                                        </a>
                                                    </Link>
                                                </li>
                                                <li
                                                    className={styles.region}
                                                >
                                                    <EnvironmentOutlined style={{ marginRight: 3 }} />
                                                    {item.region && item.region.name
                                                        ? item.region.name
                                                        : null}
                                                </li>
                                                <li
                                                    className={styles.salary}
                                                >
                                                    {/* <Icon type="dollar" style={{ marginRight: 3 }} /> */}
                                                    <DollarOutlined style={{ marginRight: 3 }} />
                                                    <span className={styles.salaryLabel}>
                                                        <b>
                                                            {convertFullSalary(item.minSalary, item.minSalaryUnit,
                                                                item.maxSalary, item.maxSalaryUnit)}
                                                        </b>
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                {" "}
                            </Col>
                        );
                    })
                    : null}
                {topJob.totalItems / topJob.pageSize > 1 ?
                    <Col span={24} style={{ textAlign: "center" }}>
                        <Pagination
                            current={currentPage}
                            pageSize={topJob.pageSize}
                            total={topJob.totalItems}
                            style={{ margin: "25px 0px 10px" }}
                            onChange={this.changePage}
                        />
                    </Col> : null}
            </Row>


        );
    }
}

const mapStateToProps = (state) => ({
    topJob: state.EventHotJobResults.data,
    currentPage: state.EventHotJobResults.currentPage
    // param: state.DetailEvent.param,
});

const mapDispatchToProps = (dispatch) => ({
    getEventHotJob: (pageIndex?: number, pageSize?: number) =>
        dispatch({ type: REDUX_SAGA.EVENT.JOB.HOT, pageIndex, pageSize }),
    setCurrentPage: (currentPage?: number) => dispatch({ type: REDUX.EVENT.JOB.HOT_CURRENT_PAGE, currentPage })
});

export default connect(mapStateToProps, mapDispatchToProps)(TopJob);
