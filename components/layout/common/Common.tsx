import React, { CSSProperties } from 'react';


export function JobType(props: {children?: any, width?: any, fontSize?: any}) {
    let style: CSSProperties = {
        color: 'black',
        backgroundColor: 'white',
        fontSize: props.fontSize ? props.fontSize : '0.8em',
        textAlign: 'center',
        width: props.width ? props.width : '75px',
        display: 'inline-block',
        borderRadius: '5px'
    };
    let label;
    switch (props.children) {
        case 'FULLTIME':
            style.color = 'white';
            style.backgroundColor = '#06bbe4';
            label = 'FULL-TIME';
            break;

        case 'PARTTIME':
            style.color = 'white';
            style.backgroundColor = '#00b33c';
            label = 'PART-TIME';
            break;

        case 'INTERNSHIP':
            style.color = 'white';
            style.backgroundColor = '#ff9933';
            label = 'THỰC TẬP';
            break;

        default:
            break;
    }

    return <div style={style}>{label}</div>
}
export const IptLetter = (props?: { value?: any }) => {
    return <span className="important-letter">{" " + props.value + " "}</span>
}
export const NotUpdate = (props?: { msg?: string }) => {
    const { msg } = props;
    return <span style={{ fontSize: '0.8 rem', fontStyle: 'italic' }}>{msg ? msg : 'Chưa cập nhật'}</span>
}