import React, { useEffect } from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import Link from 'next/link';
//@ts-ignore
import logo from "../../assets/image/logo-02.png";
import { TagOutlined, HomeOutlined, AndroidFilled, AppleFilled, BookOutlined } from '@ant-design/icons'
import styles from './Header.module.scss';
import { Menu, Dropdown } from 'antd';
import { useDispatch, useSelector } from 'react-redux'
import { REDUX_SAGA } from '../../const/actions'
import { wrapper } from '../../store'
import { END } from 'redux-saga';
import { connect } from "react-redux";
import {idHPC, idTMU, idTLU} from '../../const/schoolIDs'
//tmu
const branchEducationTMU = [
    { name: "Quản trị kinh doanh", id: 2, fts: '"Kinh doanh"' },
    { name: "Quản trị khách sạn", id: 5, fts: '"Lễ tân" "hành chính"', },
    { name: "Quản trị dịch vụ du lịch và lữ hành", id: 5, },
    { name: "Marketing", id: 9, fts: "marketing", },
    { name: "Logistics và Quản lý chuỗi cung ứng", id: -1, majorID: [33], fts: 'logistics' },
    { name: "Kế toán - Kiểm toán", id: 8, fts: '"Kế toán"' },
    { name: "Kinh doanh quốc tế", id: 2, fts: '"Kinh doanh"' },
    // { name: "Kinh tế quốc tế", id: 31 }, 
]
const branchEducationTMU2 = [
    { name: "Quản lý kinh tế", id: 31, fts: '"thị trường" "đầu tư" "chứng khoán" "phân tích" "nghiệp vụ" "tài chính" "tư vấn tài chính"', },
    { name: "Tài chính - Ngân hàng", id: 3, fts: '"tín dụng" "giao dịch" "rủi ro"	"ngân hàng" "tài chính" "tư vấn tài chính" "cá nhân"', },
    { name: "Thương mại điện tử", id: 2, fts: '"commerce" "ecommerce" "thương mại điện tử" "Shopee"' },
    { name: "Tiếng Anh thương mại", id: 22, searchByHighLight: true },
    { name: "Luật kinh tế", id: -1, majorID: 7, fts: '"pháp lý" "pháp chế" "luật" "hồ sơ"', },
    { name: "Hệ thống thông tin quản lý", id: 1, majorID: [329] },
    { name: "Quản trị nhân lực", id: 6, fts: '"Nhân sự" "HR" "Tuyển dụng" "hành chính"', }]

const Header = ({ logo, primaryColor, primaryDarkColor, param, schoolID, getEventHotJob }) => {
    const dispatch = useDispatch()
    // useEffect(() => {
    //     console.log('vao useEffect')
    //     dispatch({type: REDUX_SAGA.EVENT.JOB.HOT})
    // }, []);
    const clickBranchTMU = (e) => {
        // dispatch({type: REDUX_SAGA.EVENT.JOB.HOT})
        // getEventHotJob(0,21)
    }

    return (
        <>
            <div className={styles.header} style={{ backgroundColor: primaryColor }}>
                
                <div className={styles.directPage} style={{ left: '7%' }}>
                    <div style={{ display: "block" }}>
                        <a href='/' style={{ backgroundColor: 'rgb(0, 125, 187)' }}>
                            <TagOutlined /> Trang chủ</a>
                        <a href='/'>
                            <HomeOutlined /> Việc làm Works.vn</a>
                        {/* <a href='/result' style={{ display: isAuthen ? "" : 'none' }}><Icon type={'search'} />Tìm kiếm</a> */}
                        <a href='https://play.google.com/store/apps/details?id=com.worksvn.student&hl=en_US' target='_blank' rel="noopener noreferrer">
                            <AndroidFilled style={{ fontSize: '16.3px' }} /> Ứng dụng Android
                         </a>
                        <a href='https://apps.apple.com/vn/app/worksvn-sinh-vi%C3%AAn/id1492437454' target='_blank' rel="noopener noreferrer">
                            <AppleFilled style={{ fontSize: '16.8px' }} /> Ứng dụng iOS
                        </a>
                        <Dropdown overlay={
                            <div style={{ display: 'flex' }}>
                                <Menu>
                                    {branchEducationTMU.map((e, i) => {
                                        return (
                                            <Menu.Item key={i}>
                                                <a rel="noopener noreferrer" href="#id_schedule_a" onClick={clickBranchTMU.bind(null, e)}>
                                                    {e.name}
                                                </a>
                                            </Menu.Item>
                                        )
                                    })}
                                </Menu>
                                <Menu>
                                    {branchEducationTMU2.map((e, i) => {
                                        return (
                                            <Menu.Item key={i}>
                                                <a rel="noopener noreferrer" href="#id_schedule_a" onClick={clickBranchTMU.bind(null, e)}>
                                                    {e.name}
                                                </a>
                                            </Menu.Item>
                                        )
                                    })}
                                </Menu>
                            </div>
                        } trigger={['click']}>
                            <a className="wapper-education">
                                <BookOutlined /> Việc làm theo nhóm ngành
                            </a>
                        </Dropdown>

                    </div>

                </div>
                <div className={styles.function}
                    style={{ display: "flex" }}
                >
                    <span className={`${styles.labelLogin} hidden-mobile`} >
                        <a>
                            Đăng nhập / Đăng ký
                        </a>

                    </span>
                </div>
            </div>
        </>
    )
}


// export default Header

const mapStateToProps = (state) => ({
    logo: state.DetailEvent.logo,
    primaryColor: state.DetailEvent.primaryColor,
    primaryDarkColor: state.DetailEvent.primaryDarkColor,
    param: state.DetailEvent.param,
    schoolID: state.DetailEvent.schoolID
});

const mapDispatchToProps = (dispatch) => ({
    getEventHotJob: (pageIndex?: number, pageSize?: number) =>
        dispatch({ type: REDUX_SAGA.EVENT.JOB.HOT, pageIndex, pageSize }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
