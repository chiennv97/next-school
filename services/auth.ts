import {ISSERVER} from '../const/check-server'
// No info header
export const noInfoHeader = {
    'Access-Control-Allow-Headers': '*',
    "Content-Type": "application/json",
}

// Check invalid header
export const authHeaders = {
    'Access-Control-Allow-Headers': '*',
    'Authorization': `Bearer ${!ISSERVER ? localStorage.getItem("accessToken") : null}`,
}
