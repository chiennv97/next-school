import dynamic from 'next/dynamic'
// import JobProperties from '../../components/job/job-detail/job-properties/JobProperties';
const JobProperties = dynamic(() => import('../../components/job/job-detail/job-properties/JobProperties'))

const JobDetail = () => {
    const jobDetail = {
        "id": "d5f2fd50-e9b0-4857-8065-aa603e5e827d",
        "jobName": {
          "id": 902,
          "name": "Nhân viên kế toán",
          "jobGroup": {
            "id": 2,
            "name": "Kinh tế",
            "priority": 0,
            "imageUrl": null
          }
        },
        "jobTitle": "Nhân Viên Kế Toán",
        "address": "85/02 P. Hạ Đình, Thanh Xuân Trung, Thanh Xuân, Hà Nội",
        "region": {
          "id": 24,
          "name": "Hà Nội"
        },
        "lat": 20.9914894,
        "lon": 105.809196,
        "distance": -1,
        "employerBranchID": "64f3d4f6-faaa-4a25-8fa1-faafd3fa673b",
        "employerBranchName": "Cơ sở chính",
        "employerID": "5c4a57b2-2f95-48d8-a84b-428122da6ac1",
        "employerName": "CÔNG TY TNHH HÓA CHẤT VÀ XNK THANH BÌNH",
        "employerLogoUrl": "https://storage.googleapis.com/worksvn-prod.appspot.com/employers/5c4a57b2-2f95-48d8-a84b-428122da6ac1/avatar_1608879248197",
        "employerVerified": false,
        "createdDate": 1608879534000,
        "expirationDate": 1611421200000,
        "expired": false,
        "timeLeft": "Còn 7 ngày 4 giờ",
        "priority": [],
        "jobType": "FULLTIME",
        "minSalary": 0,
        "minSalaryUnit": null,
        "maxSalary": 0,
        "maxSalaryUnit": null,
        "applyState": null,
        "offerState": null,
        "saved": false,
        "titleHighlight": false,
        "schoolConnected": false,
        "view": 4,
        "employerCoverUrl": "https://storage.googleapis.com/worksvn-prod.appspot.com/employers/5c4a57b2-2f95-48d8-a84b-428122da6ac1/cover_1608879251444",
        "description": "1. MÔ TẢ CÔNG VIỆC : \n-\tSử dụng thành thạo phần mềm kế toán \n-\tHạch toán sổ sách & làm thạo các báo cáo thuế, báo cáo gửi ngân hàng \n-\tLập báo cáo nội bộ\n-\tChi tiết công việc trao đổi thêm khi phỏng vấn\n2. YÊU CẦU : \n-\tCó kinh nghiệm làm việc trên 10 năm chuyên nghiệp vụ kế toán cho công ty hoạt động trong lĩnh vực XNK hàng hóa. \n-\tGiới tính: Nữ\n3. QUYỀN LỢI : \n- Được hưởng đầy đủ các chế độ bảo hiểm xã hội và các quyền lợi của người lao động theo quy định hiện hành của Công ty và luật Lao động.\n- Mức lương cạnh tranh+ Thưởng hấp dẫn theo quy chế của công ty.",
        "requiredSkills": [
          {
            "id": 19,
            "name": "Tỉ mỉ, cẩn thận"
          }
        ],
        "requiredLanguages": [],
        "requiredWorkingTools": [],
        "shifts": [
          {
            "id": "7d31ea7d-56ab-4b9e-bf27-a5d0c3ce7a2f",
            "startTime": "08:00",
            "endTime": "17:00",
            "minSalary": 0,
            "maxSalary": 0,
            "unit": null,
            "mon": true,
            "tue": true,
            "wed": true,
            "thu": true,
            "fri": true,
            "sat": true,
            "sun": false,
            "genderRequireds": [
              {
                "id": "73523f9d-db70-4ede-afc1-804169c27fb1",
                "gender": "FEMALE",
                "quantity": 1,
                "applied": 0
              }
            ]
          }
        ]
      }
    return <>
      <JobProperties jobDetail={jobDetail}/>
    </>
  }
  export default JobDetail
  