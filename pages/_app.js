import { wrapper } from '../store'
import { useEffect, useState } from 'react'
import '../public/css/global.scss'
import 'antd/dist/antd.css';
import { useDispatch } from 'react-redux'
import { REDUX_SAGA, REDUX } from '../const/actions'
import Router, { useRouter } from 'next/router'
import {_get} from '../services/base-api'
import {PUBLIC_HOST} from '../environment/development'
import {noInfoHeader} from '../services/auth'

function App({ Component, pageProps }) {
  const dispatch = useDispatch()
  // const router = useRouter()
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    if ('scrollRestoration' in window.history) {
      window.history.scrollRestoration = 'manual';
      let shouldScrollRestore;
      let cachedScrollPositions = []
      Router.events.on('routeChangeStart', () => {
        cachedScrollPositions.push([window.scrollX, window.scrollY]);
      });

      Router.events.on('routeChangeComplete', () => {
        if (shouldScrollRestore) {
          const { x, y } = shouldScrollRestore;
          window.scrollTo(x, y);
          shouldScrollRestore = false;
        }
      });

      Router.beforePopState(() => {
        const [x, y] = cachedScrollPositions.pop();
        shouldScrollRestore = { x, y };

        return true;
      });
    }
  }, []);
  
  useEffect(() => {
    dispatch({ type: REDUX_SAGA.EVENT.JOB.HOT })
    // console.log(window.location.search)
    let searchParams = new URLSearchParams(window.location.search);
    let dataParams = searchParams.get("data")
    if(dataParams) {
      let decodeData = window.atob(dataParams)
      let newDataParams = new URLSearchParams(decodeData);
      let schoolID = newDataParams.get("schoolID")
      let eventID = newDataParams.get("eventID")
      console.log(schoolID)
      console.log(eventID)
      if (schoolID && eventID) {
        getInfoSchool(schoolID).then((res) => {
          if (res && res.data) {
            // this.props.setInfoEvent(res.data.logoUrl, res.data.primaryColor, res.data.primaryDarkColor, window.location.search, queryParam2.schoolID, queryParam2.eventID, res.data.name)
            dispatch({
              type: REDUX.EVENT.LOGO_SCHOOL,
              res: res.data.logo,
              primaryColor: res.data.primaryColor,
              primaryDarkColor: res.data.primaryDarkColor,
              param: window.location.search,
              schoolID,
              eventID,
              name: res.data.name
            });
          }
        }).finally(() => {
          setLoading(false)
        })
      } else {
        setLoading(false)
      }
    } else {
      setLoading(false)
    }
    console.log(dataParams)
  }, []);
  function getInfoSchool(schoolID) {
    let res = _get(
      null,
      `/api/schools/${schoolID}/simple`,
      PUBLIC_HOST,
      noInfoHeader
    )
    return res
  }
  return <Component {...pageProps} />
}

export default wrapper.withRedux(App)

// initRouterListeners();

// function initRouterListeners() {

//     console.log("Init router listeners");

//     const routes = [];

//     Router.events.on('routeChangeStart', (url) => {
//         pushCurrentRouteInfo();
//     });

//     Router.events.on('routeChangeComplete', (url) => {
//         fixScrollPosition();
//     });


//     // Hack to set scrollTop because of this issue:
//     // - https://github.com/zeit/next.js/issues/1309
//     // - https://github.com/zeit/next.js/issues/3303

//     function pushCurrentRouteInfo() {
//         routes.push({pathname: Router.pathname, scrollY: window.scrollY});
//     }

//     // TODO: We guess we're going back, but there must be a better way
//     // https://github.com/zeit/next.js/issues/1309#issuecomment-435057091
//     function isBack() {
//         return routes.length >= 2 && Router.pathname === routes[routes.length - 2].pathname;
//     }

//     function fixScrollPosition () {

//         let scrollY = 0;

//         if (isBack()) {
//             routes.pop(); // route where we come from
//             const targetRoute = routes.pop(); // route where we return
//             scrollY = targetRoute.scrollY; // scrollY we had before
//         }

//         console.log("Scrolling to", scrollY);
//         window.requestAnimationFrame(() => window.scrollTo(0, scrollY));
//         console.log("routes now:", routes);
//     }
// }