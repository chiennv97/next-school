import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { END } from 'redux-saga'
import { wrapper } from '../store'
import Head from "next/head";
import Header from '../components/header/Header'
import Banner from '../components/banner/Banner'
import TopJob from '../components/top-job-em/TopJob'
import { REDUX_SAGA } from '../const/actions'
import Test from '../components/test/test'
const Index = () => {
  const dispatch = useDispatch()

    useEffect(() => {
      console.log('vao useEffect Index')
  }, []);
  // const {data} = useSelector(state => state.EventHotJobResults);
  // console.log(data)
  return <>
    <Head>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
      <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/jquery.marquee@1.5.0/jquery.marquee.min.js'></script>
    </Head>
    <Header />
    <Banner />
    <TopJob />
    <Test />
  </>
}
// export const getServerSideProps = wrapper.getServerSideProps(async ({store}) => {
//   store.dispatch({type: REDUX_SAGA.EVENT.JOB.HOT});
//   store.dispatch(END);
//   await store.sagaTask.toPromise();

// });
export default Index
