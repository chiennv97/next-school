const withImages = require('next-images')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const DuplicatePackageCheckerPlugin = require('duplicate-package-checker-webpack-plugin')
const CompressionPlugin = require("compression-webpack-plugin");
module.exports = withImages({
  webpack(config, options) {
    config.plugins.push(new DuplicatePackageCheckerPlugin())
    config.plugins.push(new CompressionPlugin(
      {
        filename: "[path][base].gz",
        algorithm: "gzip",
        test: /\.js$|\.css$|\.html$/,
        threshold: 10240,
        minRatio: 0.8,
      }
    ))

      // config.plugins.push(
      //   new BundleAnalyzerPlugin({
      //     analyzerMode: 'server',
      //     analyzerPort: 8889,
      //     openAnalyzer: true,
      //   })
      // )
     
    return config
  }
})