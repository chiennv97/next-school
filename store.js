import { applyMiddleware, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { createWrapper } from 'next-redux-wrapper'

import myReducer from './redux/store/reducer'
import saga from './redux/store/saga';


const bindMiddleware = (middleware) => {
  // if (process.env.NODE_ENV !== 'production') {
  //   const { composeWithDevTools } = require('redux-devtools-extension')
  //   return composeWithDevTools(applyMiddleware(...middleware))
  // }
  return applyMiddleware(...middleware)
}
const sagaMiddleware = createSagaMiddleware()
export const store = createStore(myReducer, bindMiddleware([sagaMiddleware])) 
export const makeStore = (context) => {
  
  

  store.sagaTask = sagaMiddleware.run(saga)

  return store
}

export const wrapper = createWrapper(makeStore, { debug: true })
